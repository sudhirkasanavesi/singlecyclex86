/*
 * Copyright (c) 2012 Google
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Gabe Black
 */

#ifndef _CORE_CSE502_H
#define _CORE_CSE502_H

#include "core.h"
#include "types.h"
#include <stdlib.h>
#include <map>
#include <queue>
#include <bitset>
#include <stack>



class CoreCSE502 : public Core {
	typedef CoreCSE502 SC_CURRENT_USER_MODULE;

public:

        enum State {
            ResetState,
            FromCacheState,
            PrefixState,
            OpcodeState,
            ModRMState,
            SIBState,
            DisplacementState,
            ImmediateState,
            //We should never get to this state. Getting here is an error.
            ErrorState
        };

        enum Prefixes {
        	NoOverride,
        	ESOverride,
        	CSOverride,
        	SSOverride,
        	DSOverride,
        	FSOverride,
        	GSOverride,
        	RexPrefix,
        	OperandSizeOverride,
        	AddressSizeOverride,
        	Lock,
        	Rep,
        	Repne
        };

        enum Sizes {
        	Bit16 = 16,
        	Bit32 = 32,
        	Bit64 = 64
        };

        enum SizeType {
            NoImm,
            NI = NoImm,
            ByteImm,
            BY = ByteImm,
            WordImm,
            WO = WordImm,
            DWordImm,
            DW = DWordImm,
            QWordImm,
            QW = QWordImm,
            OWordImm,
            OW = OWordImm,
            VWordImm,
            VW = VWordImm,
            ZWordImm,
            ZW = ZWordImm,
            //The enter instruction takes -2- immediates for a total of 3 bytes
            Enter,
            EN = Enter,
            Pointer,
            PO = Pointer
        };
	
        enum usesModRM {
        	No,
        	Yes
        };

        enum d_cache_states {
            Normal_State,
            Read_State
        };

        map<string, uint64_t> register_map_file;
        map<string, Prefixes> inst_prefix_table;
        map<string, Sizes> default_operand_size_table;
        map<string, Sizes> address_size_table;
        map<string, SizeType> one_byte_immediate_type_table;
        map<string, SizeType> two_byte_immediate_type_table;
        map<string, usesModRM> one_byte_uses_modrm;
        map<string, usesModRM> two_byte_uses_modrm;
	map<string, uint64_t> opsize_table;
	map<string, uint64_t> addrsize_table;
	struct dcache_copy dcopy;
	bool dcache_read_request, cache_read_done;

        static const uint8_t SizeTypeToSize[3][10];

        Instruction current_instruction;
        State state;
        int offset;
        int immediateSize;
        int displacementSize;
        int immediateCollected;
        bool instDone;
        bool outOfBytes; 
        bool firstRunFlag;
        sc_addr prev_tag[i_ports];
        sc_inst fetch_decode_shared_buffer;
        d_cache_states d_cache_state;

        queue<Instruction> inst_queue;
		stack<uint64_t> system_stack;
		int to_add_inst_size;
    	uint64_t last_inst_address;
    	uint64_t last_inst_size;
    	bool jump_flag_for_decoder;
		struct control_flags flag;
        void modifyRIP(uint64_t);
        bool fetch();
        bool decode();
        void resetEverythingForNextInstruction();
		void process_instruction();
		void print_instr_content(Instruction instr);
        virtual void do_reset();
        virtual void work();
	void process_instr(Instruction instr);
	void execute(struct execution_struct &exec);
	uint64_t calculate_memory_address(Instruction instr);
	uint64_t special_process_sib(Instruction instr);
	uint64_t process_sib(Instruction instr);
    void handle_sub(struct execution_struct &exec);
    void handle_neg(struct execution_struct &exec);
	void handle_xor(struct execution_struct &exec);
	void handle_and(struct execution_struct &exec);
	void handle_move(struct execution_struct &exec);
	void handle_call_abs(struct execution_struct &exec);
	void handle_syscall(struct execution_struct &exec);
	void handle_jb();
	void handle_retq();
	void write_back(uint64_t address, uint64_t data);
	uint64_t read_memory(uint64_t address);
	void dump_registers();	
	void handle_increment(struct execution_struct &exec);
	void handle_decrement(struct execution_struct &exec);
	void handle_jump(struct execution_struct &exec);
	void handle_push(struct execution_struct &exec);
	void handle_pop(struct execution_struct &exec);
	void handle_immed_move(struct execution_struct &exec);
	void handle_shiftr(struct execution_struct &exec);
	void handle_shiftl(struct execution_struct &exec);
	void handle_shiftl_one(struct execution_struct &exec);
	void handle_shiftr_one(struct execution_struct &exec);
	void callq_reset();
    void handle_test(struct execution_struct &exec);
    void handle_add(struct execution_struct &exec);
	void handle_call_relative(struct execution_struct &exec);
	void handle_ja(struct execution_struct &exec);
	void handle_je(struct execution_struct &exec);
	void handle_jne(struct execution_struct &exec);
	void handle_jb(struct execution_struct &exec);
    void handle_jge(struct execution_struct &exec);
    void handle_jle(struct execution_struct &exec);
    void handle_jl(struct execution_struct &exec);
    void handle_jg(struct execution_struct &exec);    
	void handle_lea(struct execution_struct &exec);
	void handle_nopl(struct execution_struct &exec);
	void handle_cmovne(struct execution_struct &exec);
	void handle_cmovns(struct execution_struct &exec);
	void handle_cmovg(struct execution_struct &exec);
	void handle_compare(struct execution_struct &exec);
	void handle_sar(struct execution_struct &exec);
	void handle_shl(struct execution_struct &exec);
	void handle_shr(struct execution_struct &exec);
    void handle_idiv(struct execution_struct &exec);
    void handle_imul(struct execution_struct &exec);
	void debug_stack_print();
	void handle_jump_absolute(struct execution_struct &exec);

        std::string getNextByte()
        {
            return fetch_decode_shared_buffer.range(offset, offset-7).to_string();
        }

        void moveOffset()
        {
        	offset = offset - 8;
            if (offset < 7)
                outOfBytes = true;
        }

        void moveOffset(uint8_t bytes)
        {
            offset = offset - (8*bytes);
            if (offset < 7)
                outOfBytes = true;
        }

        void getImmediate(int &collected, uint64_t &current, int size)
        {
        	//Figure out how many bytes we still need to get for the
        	//immediate.
        	int toGet = size - collected;
        	//Figure out how many bytes are left in our "buffer"
        	int remaining = (offset + 1)/8; // size of buffer: 16
        	//Get as much as we need, up to the amount available.
        	toGet = toGet > remaining ? remaining : toGet;

            std::string temp_string;
            for (int i = toGet; i > 0; i--)
            {
            	temp_string = getNextByte() + temp_string;
            	moveOffset();
            	//cout << "after moving offset in getimm() " << getNextByte()<<endl;
            }

        	//Shift the bytes we want to be all the way to the right
            char *end;
        	uint64_t partialImm = std::strtoull(temp_string.c_str(),&end,2);  //fetchChunk >> (offset * 8);
        	//Mask off what we don't want
        	//partialImm &= mask(toGet * 8);
        	//Shift it over to overlay with our displacement.
        	partialImm <<= (immediateCollected * 8);
        	//Put it into our displacement
        	current |= partialImm;
        	//Update how many bytes we've collected.
        	collected += toGet;
        	//consumeBytes(toGet);
    	}	

        //Functions to handle each of the states
        State doResetState();
        //State doFromCacheState();
        State doPrefixState(std::string);
        State doOpcodeState(std::string);
        State doModRMState(std::string);
        State doSIBState(std::string);
        State doDisplacementState();
        State doImmediateState();

        CoreCSE502(const sc_module_name& name)
		:	Core(name)
        {
	        //cout << "RSP : "<< RSP.to_uint64()<<endl;
            d_cache_state = Normal_State;
            jump_flag_for_decoder = false;
	        last_inst_size = 0;
	        to_add_inst_size = 0;
	        last_inst_address = RIP.to_uint64();
            firstRunFlag = true;
            current_instruction.rex.w = current_instruction.rex.r = current_instruction.rex.x = current_instruction.rex.b = 0;
            current_instruction.opcode.num = 0;
            current_instruction.opcode.prefixA = current_instruction.opcode.prefixB = current_instruction.opcode.op = "";
            immediateCollected = 0;
            immediateSize = 0;
            displacementSize = 0;
            current_instruction.immediate = 0;
            current_instruction.displacement = 0;
            current_instruction.dispSize = 0;
            current_instruction.opSize = 0;
            current_instruction.addrSize = 0;
            current_instruction.stackSize = 0;
            current_instruction.dispSize = 0;
            current_instruction.modRM.mod = current_instruction.modRM.reg = current_instruction.modRM.rm = "";
            current_instruction.sib.scale = current_instruction.sib.base = current_instruction.sib.index = "";
            current_instruction.immed_flag = false;

	    opsize_table.insert(make_pair("000",2));
	    opsize_table.insert(make_pair("001",2));
	    opsize_table.insert(make_pair("010",1));	
	    opsize_table.insert(make_pair("011",1));	
	    opsize_table.insert(make_pair("100",3));
	    opsize_table.insert(make_pair("101",3));
	    opsize_table.insert(make_pair("110",3));
	    opsize_table.insert(make_pair("111",3));    

	    addrsize_table.insert(make_pair("000",3));
	    addrsize_table.insert(make_pair("001",2));
	    addrsize_table.insert(make_pair("010",3));
            addrsize_table.insert(make_pair("011",2));
            addrsize_table.insert(make_pair("100",3));
            addrsize_table.insert(make_pair("101",2));
            addrsize_table.insert(make_pair("110",3));
            addrsize_table.insert(make_pair("111",2));

            // inst_prefix table 
            inst_prefix_table.insert(make_pair("00100110", ESOverride));
            inst_prefix_table.insert(make_pair("00101110", CSOverride));
            inst_prefix_table.insert(make_pair("00110110", SSOverride));
            inst_prefix_table.insert(make_pair("00111110", DSOverride));

            inst_prefix_table.insert(make_pair("01000000", RexPrefix));
            inst_prefix_table.insert(make_pair("01000001", RexPrefix));
            inst_prefix_table.insert(make_pair("01000010", RexPrefix));

            inst_prefix_table.insert(make_pair("01000011", RexPrefix));
            inst_prefix_table.insert(make_pair("01000100", RexPrefix));
            inst_prefix_table.insert(make_pair("01000101", RexPrefix));
            inst_prefix_table.insert(make_pair("01000110", RexPrefix));

            inst_prefix_table.insert(make_pair("01000111", RexPrefix));
            inst_prefix_table.insert(make_pair("01001000", RexPrefix));
            inst_prefix_table.insert(make_pair("01001001", RexPrefix));
            inst_prefix_table.insert(make_pair("01001010", RexPrefix)); 

            inst_prefix_table.insert(make_pair("01001011", RexPrefix));  
            inst_prefix_table.insert(make_pair("01001100", RexPrefix));  
            inst_prefix_table.insert(make_pair("01001101", RexPrefix));
            inst_prefix_table.insert(make_pair("01001110", RexPrefix));
            inst_prefix_table.insert(make_pair("01001111", RexPrefix)); 

            inst_prefix_table.insert(make_pair("01100100", FSOverride)); 
            inst_prefix_table.insert(make_pair("01100101", GSOverride));  
            inst_prefix_table.insert(make_pair("01100110", OperandSizeOverride));

            inst_prefix_table.insert(make_pair("01100111", AddressSizeOverride));  
            inst_prefix_table.insert(make_pair("11110000", Lock));  
            inst_prefix_table.insert(make_pair("11110010", Repne)); 
            inst_prefix_table.insert(make_pair("11110011", Rep)); 

            // Default operand and address size
            // lookup in this map using a string REX.W Prefix+Operand-Size Prefix 66H+Address-Size Prefix 67H

            default_operand_size_table.insert(make_pair("000", Bit32));
            default_operand_size_table.insert(make_pair("001", Bit32));
            default_operand_size_table.insert(make_pair("010", Bit16));
            default_operand_size_table.insert(make_pair("011", Bit16));
            default_operand_size_table.insert(make_pair("100", Bit64));
            default_operand_size_table.insert(make_pair("101", Bit64));
            default_operand_size_table.insert(make_pair("110", Bit64));
            default_operand_size_table.insert(make_pair("111", Bit64));

            address_size_table.insert(make_pair("000", Bit64));
            address_size_table.insert(make_pair("001", Bit32));
            address_size_table.insert(make_pair("010", Bit64));
            address_size_table.insert(make_pair("011", Bit32));
            address_size_table.insert(make_pair("100", Bit64));
            address_size_table.insert(make_pair("101", Bit32));
            address_size_table.insert(make_pair("110", Bit64));
            address_size_table.insert(make_pair("111", Bit32));

            one_byte_immediate_type_table.insert(make_pair("00000100", BY)); // 0x04
            one_byte_immediate_type_table.insert(make_pair("00000101", ZW)); // 0x05
            one_byte_immediate_type_table.insert(make_pair("00001100", BY)); // 0x0C
            one_byte_immediate_type_table.insert(make_pair("00001101", ZW)); // 0x0D

            one_byte_immediate_type_table.insert(make_pair("00010100", BY)); // 0x14
            one_byte_immediate_type_table.insert(make_pair("00010101", ZW)); // 0x15
            one_byte_immediate_type_table.insert(make_pair("00011100", BY)); // 0x1C
            one_byte_immediate_type_table.insert(make_pair("00011101", ZW)); // 0x1D

            one_byte_immediate_type_table.insert(make_pair("00100100", BY)); // 0x24
            one_byte_immediate_type_table.insert(make_pair("00100101", ZW)); // 0x25
            one_byte_immediate_type_table.insert(make_pair("00101100", BY)); // 0x2C
            one_byte_immediate_type_table.insert(make_pair("00101101", ZW)); // 0x2D

            one_byte_immediate_type_table.insert(make_pair("00110100", BY)); // 0x34
            one_byte_immediate_type_table.insert(make_pair("00110101", ZW)); // 0x35
            one_byte_immediate_type_table.insert(make_pair("00111100", BY)); // 0x3C
            one_byte_immediate_type_table.insert(make_pair("00111101", ZW)); // 0x3D

            one_byte_immediate_type_table.insert(make_pair("01101000", ZW)); // 0x68
            one_byte_immediate_type_table.insert(make_pair("01101001", ZW)); // 0x69
            one_byte_immediate_type_table.insert(make_pair("01101010", BY)); // 0x6A
            one_byte_immediate_type_table.insert(make_pair("01101011", BY)); // 0x6B

            one_byte_immediate_type_table.insert(make_pair("01110000", BY)); //0x70
            one_byte_immediate_type_table.insert(make_pair("01110001", BY)); //0x71
            one_byte_immediate_type_table.insert(make_pair("01110010", BY)); //0x72
            one_byte_immediate_type_table.insert(make_pair("01110011", BY)); //0x73
            one_byte_immediate_type_table.insert(make_pair("01110100", BY)); //0x74
            one_byte_immediate_type_table.insert(make_pair("01110101", BY)); //0x75
            one_byte_immediate_type_table.insert(make_pair("01110110", BY)); //0x76
            one_byte_immediate_type_table.insert(make_pair("01110111", BY)); //0x77
            one_byte_immediate_type_table.insert(make_pair("01111000", BY)); //0x78
            one_byte_immediate_type_table.insert(make_pair("01111001", BY)); //0x79
            one_byte_immediate_type_table.insert(make_pair("01111010", BY)); //0x7A
            one_byte_immediate_type_table.insert(make_pair("01111011", BY)); //0x7B
            one_byte_immediate_type_table.insert(make_pair("01111100", BY)); //0x7C
            one_byte_immediate_type_table.insert(make_pair("01111101", BY)); //0x7D
            one_byte_immediate_type_table.insert(make_pair("01111110", BY)); //0x7E
            one_byte_immediate_type_table.insert(make_pair("01111111", BY)); //0x7F  

            one_byte_immediate_type_table.insert(make_pair("10000000", BY)); //0x80
            one_byte_immediate_type_table.insert(make_pair("10000001", ZW)); //0x81
            one_byte_immediate_type_table.insert(make_pair("10000010", BY)); //0x82
            one_byte_immediate_type_table.insert(make_pair("10000011", BY)); //0x83     

            one_byte_immediate_type_table.insert(make_pair("10100000", VW)); //0xA0
            one_byte_immediate_type_table.insert(make_pair("10100001", VW)); //0xA1
            one_byte_immediate_type_table.insert(make_pair("10100010", VW)); //0xA2
            one_byte_immediate_type_table.insert(make_pair("10100011", VW)); //0xA3   
            one_byte_immediate_type_table.insert(make_pair("10101000", BY)); //0xA8
            one_byte_immediate_type_table.insert(make_pair("10101001", ZW)); //0xA9

            one_byte_immediate_type_table.insert(make_pair("10110000", BY)); //0xB0
            one_byte_immediate_type_table.insert(make_pair("10110001", BY)); //0xB1
            one_byte_immediate_type_table.insert(make_pair("10110010", BY)); //0xB2
            one_byte_immediate_type_table.insert(make_pair("10110011", BY)); //0xB3
            one_byte_immediate_type_table.insert(make_pair("10110100", BY)); //0xB4
            one_byte_immediate_type_table.insert(make_pair("10110101", BY)); //0xB5
            one_byte_immediate_type_table.insert(make_pair("10110110", BY)); //0xB6
            one_byte_immediate_type_table.insert(make_pair("10110111", BY)); //0xB7
            one_byte_immediate_type_table.insert(make_pair("10111000", VW)); //0xB8
            one_byte_immediate_type_table.insert(make_pair("10111001", VW)); //0xB9
            one_byte_immediate_type_table.insert(make_pair("10111010", VW)); //0xBA
            one_byte_immediate_type_table.insert(make_pair("10111011", VW)); //0xBB
            one_byte_immediate_type_table.insert(make_pair("10111100", VW)); //0xBC
            one_byte_immediate_type_table.insert(make_pair("10111101", VW)); //0xBD
            one_byte_immediate_type_table.insert(make_pair("10111110", VW)); //0xBE
            one_byte_immediate_type_table.insert(make_pair("10111111", VW)); //0xBF  

            one_byte_immediate_type_table.insert(make_pair("11000000", BY)); //0xC0
            one_byte_immediate_type_table.insert(make_pair("11000001", BY)); //0xC1
            one_byte_immediate_type_table.insert(make_pair("11000010", WO)); //0xC2
            one_byte_immediate_type_table.insert(make_pair("11000110", BY)); //0xC6
            one_byte_immediate_type_table.insert(make_pair("11000111", ZW)); //0xC7
            one_byte_immediate_type_table.insert(make_pair("11001000", EN)); //0xC8
            one_byte_immediate_type_table.insert(make_pair("11001010", WO)); //0xCA
            one_byte_immediate_type_table.insert(make_pair("11001101", BY)); //0xCD       

            one_byte_immediate_type_table.insert(make_pair("11010100", BY)); //0xD4
            one_byte_immediate_type_table.insert(make_pair("11010101", BY)); //0xD5     

            one_byte_immediate_type_table.insert(make_pair("11100000", BY)); //0xE0
            one_byte_immediate_type_table.insert(make_pair("11100001", BY)); //0xE1
            one_byte_immediate_type_table.insert(make_pair("11100010", BY)); //0xE2
            one_byte_immediate_type_table.insert(make_pair("11100011", BY)); //0xE3
            one_byte_immediate_type_table.insert(make_pair("11100100", BY)); //0xE4
            one_byte_immediate_type_table.insert(make_pair("11100101", BY)); //0xE5
            one_byte_immediate_type_table.insert(make_pair("11100110", BY)); //0xE6
            one_byte_immediate_type_table.insert(make_pair("11100111", BY)); //0xE7
            one_byte_immediate_type_table.insert(make_pair("11101000", ZW)); //0xE8
            one_byte_immediate_type_table.insert(make_pair("11101001", ZW)); //0xE9
            one_byte_immediate_type_table.insert(make_pair("11101010", PO)); //0xEA
            one_byte_immediate_type_table.insert(make_pair("11101011", BY)); //0xEB


            // two byte immediate
            two_byte_immediate_type_table.insert(make_pair("00000100", WO)); //0x04
            two_byte_immediate_type_table.insert(make_pair("00001111", BY)); //0x0F
            two_byte_immediate_type_table.insert(make_pair("01110000", BY)); //0x70
            two_byte_immediate_type_table.insert(make_pair("01110001", BY)); //0x71
            two_byte_immediate_type_table.insert(make_pair("01110010", BY)); //0x72
            two_byte_immediate_type_table.insert(make_pair("01110011", BY)); //0x73

            two_byte_immediate_type_table.insert(make_pair("10000000", ZW)); //0x80
            two_byte_immediate_type_table.insert(make_pair("10000001", ZW)); //0x81
            two_byte_immediate_type_table.insert(make_pair("10000010", ZW)); //0x82
            two_byte_immediate_type_table.insert(make_pair("10000011", ZW)); //0x83
            two_byte_immediate_type_table.insert(make_pair("10000100", ZW)); //0x84
            two_byte_immediate_type_table.insert(make_pair("10000101", ZW)); //0x85
            two_byte_immediate_type_table.insert(make_pair("10000110", ZW)); //0x86
            two_byte_immediate_type_table.insert(make_pair("10000111", ZW)); //0x87
            two_byte_immediate_type_table.insert(make_pair("10001000", ZW)); //0x88
            two_byte_immediate_type_table.insert(make_pair("10001001", ZW)); //0x89
            two_byte_immediate_type_table.insert(make_pair("10001010", ZW)); //0x8A
            two_byte_immediate_type_table.insert(make_pair("10001011", ZW)); //0x8B
            two_byte_immediate_type_table.insert(make_pair("10001100", ZW)); //0x8C
            two_byte_immediate_type_table.insert(make_pair("10001101", ZW)); //0x8D
            two_byte_immediate_type_table.insert(make_pair("10001110", ZW)); //0x8E
            two_byte_immediate_type_table.insert(make_pair("10001111", ZW)); //0x8F    

            two_byte_immediate_type_table.insert(make_pair("10100100", BY)); //0xA4
            two_byte_immediate_type_table.insert(make_pair("10101100", BY)); //0xAC

            two_byte_immediate_type_table.insert(make_pair("10111000", ZW)); //0xB8
            two_byte_immediate_type_table.insert(make_pair("10111010", BY)); //0xBA

            two_byte_immediate_type_table.insert(make_pair("11000010", BY)); //0xC2
            two_byte_immediate_type_table.insert(make_pair("11000100", BY)); //0xC4
            two_byte_immediate_type_table.insert(make_pair("11000101", BY)); //0xC5
            two_byte_immediate_type_table.insert(make_pair("11000110", BY)); //0xC6    

            one_byte_uses_modrm.insert(make_pair("00000000", Yes)); //0x00
            one_byte_uses_modrm.insert(make_pair("00000001", Yes)); //0x01
            one_byte_uses_modrm.insert(make_pair("00000010", Yes)); //0x02
            one_byte_uses_modrm.insert(make_pair("00000011", Yes)); //0x03
            one_byte_uses_modrm.insert(make_pair("00000100", No)); //0x04
            one_byte_uses_modrm.insert(make_pair("00000101", No)); //0x05
            one_byte_uses_modrm.insert(make_pair("00000110", No)); //0x06
            one_byte_uses_modrm.insert(make_pair("00000111", No)); //0x07
            one_byte_uses_modrm.insert(make_pair("00001000", Yes)); //0x08
            one_byte_uses_modrm.insert(make_pair("00001001", Yes)); //0x09
            one_byte_uses_modrm.insert(make_pair("00001010", Yes)); //0x0A
            one_byte_uses_modrm.insert(make_pair("00001011", Yes)); //0x0B
            one_byte_uses_modrm.insert(make_pair("00001100", No)); //0x0C
            one_byte_uses_modrm.insert(make_pair("00001101", No)); //0x0D
            one_byte_uses_modrm.insert(make_pair("00001110", No)); //0x0E
            one_byte_uses_modrm.insert(make_pair("00001111", No)); //0x0F 

            one_byte_uses_modrm.insert(make_pair("00010000", Yes)); //0x10
            one_byte_uses_modrm.insert(make_pair("00010001", Yes)); //0x11
            one_byte_uses_modrm.insert(make_pair("00010010", Yes)); //0x12
            one_byte_uses_modrm.insert(make_pair("00010011", Yes)); //0x13
            one_byte_uses_modrm.insert(make_pair("00010100", No)); //0x14
            one_byte_uses_modrm.insert(make_pair("00010101", No)); //0x15
            one_byte_uses_modrm.insert(make_pair("00010110", No)); //0x16
            one_byte_uses_modrm.insert(make_pair("00010111", No)); //0x17
            one_byte_uses_modrm.insert(make_pair("00011000", Yes)); //0x18
            one_byte_uses_modrm.insert(make_pair("00011001", Yes)); //0x19
            one_byte_uses_modrm.insert(make_pair("00011010", Yes)); //0x1A
            one_byte_uses_modrm.insert(make_pair("00011011", Yes)); //0x1B
            one_byte_uses_modrm.insert(make_pair("00011100", No)); //0x1C
            one_byte_uses_modrm.insert(make_pair("00011101", No)); //0x1D
            one_byte_uses_modrm.insert(make_pair("00011110", No)); //0x1E
            one_byte_uses_modrm.insert(make_pair("00011111", No)); //0x1F 

            one_byte_uses_modrm.insert(make_pair("00100000", Yes)); //0x20
            one_byte_uses_modrm.insert(make_pair("00100001", Yes)); //0x21
            one_byte_uses_modrm.insert(make_pair("00100010", Yes)); //0x22
            one_byte_uses_modrm.insert(make_pair("00100011", Yes)); //0x23
            one_byte_uses_modrm.insert(make_pair("00100100", No)); //0x24
            one_byte_uses_modrm.insert(make_pair("00100101", No)); //0x25
            one_byte_uses_modrm.insert(make_pair("00100110", No)); //0x26
            one_byte_uses_modrm.insert(make_pair("00100111", No)); //0x27
            one_byte_uses_modrm.insert(make_pair("00101000", Yes)); //0x28
            one_byte_uses_modrm.insert(make_pair("00101001", Yes)); //0x29
            one_byte_uses_modrm.insert(make_pair("00101010", Yes)); //0x2A
            one_byte_uses_modrm.insert(make_pair("00101011", Yes)); //0x2B
            one_byte_uses_modrm.insert(make_pair("00101100", No)); //0x2C
            one_byte_uses_modrm.insert(make_pair("00101101", No)); //0x2D
            one_byte_uses_modrm.insert(make_pair("00101110", No)); //0x2E
            one_byte_uses_modrm.insert(make_pair("00101111", No)); //0x2F 

            one_byte_uses_modrm.insert(make_pair("00110000", Yes)); //0x30
            one_byte_uses_modrm.insert(make_pair("00110001", Yes)); //0x31
            one_byte_uses_modrm.insert(make_pair("00110010", Yes)); //0x32
            one_byte_uses_modrm.insert(make_pair("00110011", Yes)); //0x33
            one_byte_uses_modrm.insert(make_pair("00110100", No)); //0x34
            one_byte_uses_modrm.insert(make_pair("00110101", No)); //0x35
            one_byte_uses_modrm.insert(make_pair("00110110", No)); //0x36
            one_byte_uses_modrm.insert(make_pair("00110111", No)); //0x37
            one_byte_uses_modrm.insert(make_pair("00111000", Yes)); //0x38
            one_byte_uses_modrm.insert(make_pair("00111001", Yes)); //0x39
            one_byte_uses_modrm.insert(make_pair("00111010", Yes)); //0x3A
            one_byte_uses_modrm.insert(make_pair("00111011", Yes)); //0x3B
            one_byte_uses_modrm.insert(make_pair("00111100", No)); //0x3C
            one_byte_uses_modrm.insert(make_pair("00111101", No)); //0x3D
            one_byte_uses_modrm.insert(make_pair("00111110", No)); //0x3E
            one_byte_uses_modrm.insert(make_pair("00111111", No)); //0x3F 

            one_byte_uses_modrm.insert(make_pair("01000000", No)); //0x40
            one_byte_uses_modrm.insert(make_pair("01000001", No)); //0x41
            one_byte_uses_modrm.insert(make_pair("01000010", No)); //0x42
            one_byte_uses_modrm.insert(make_pair("01000011", No)); //0x43
            one_byte_uses_modrm.insert(make_pair("01000100", No)); //0x44
            one_byte_uses_modrm.insert(make_pair("01000101", No)); //0x45
            one_byte_uses_modrm.insert(make_pair("01000110", No)); //0x46
            one_byte_uses_modrm.insert(make_pair("01000111", No)); //0x47
            one_byte_uses_modrm.insert(make_pair("01001000", No)); //0x48
            one_byte_uses_modrm.insert(make_pair("01001001", No)); //0x49
            one_byte_uses_modrm.insert(make_pair("01001010", No)); //0x4A
            one_byte_uses_modrm.insert(make_pair("01001011", No)); //0x4B
            one_byte_uses_modrm.insert(make_pair("01001100", No)); //0x4C
            one_byte_uses_modrm.insert(make_pair("01001101", No)); //0x4D
            one_byte_uses_modrm.insert(make_pair("01001110", No)); //0x4E
            one_byte_uses_modrm.insert(make_pair("01001111", No)); //0x4F 

            one_byte_uses_modrm.insert(make_pair("01010000", No)); //0x50
            one_byte_uses_modrm.insert(make_pair("01010001", No)); //0x51
            one_byte_uses_modrm.insert(make_pair("01010010", No)); //0x52
            one_byte_uses_modrm.insert(make_pair("01010011", No)); //0x53
            one_byte_uses_modrm.insert(make_pair("01010100", No)); //0x54
            one_byte_uses_modrm.insert(make_pair("01010101", No)); //0x55
            one_byte_uses_modrm.insert(make_pair("01010110", No)); //0x56
            one_byte_uses_modrm.insert(make_pair("01010111", No)); //0x57
            one_byte_uses_modrm.insert(make_pair("01011000", No)); //0x58
            one_byte_uses_modrm.insert(make_pair("01011001", No)); //0x59
            one_byte_uses_modrm.insert(make_pair("01011010", No)); //0x5A
            one_byte_uses_modrm.insert(make_pair("01011011", No)); //0x5B
            one_byte_uses_modrm.insert(make_pair("01011100", No)); //0x5C
            one_byte_uses_modrm.insert(make_pair("01011101", No)); //0x5D
            one_byte_uses_modrm.insert(make_pair("01011110", No)); //0x5E
            one_byte_uses_modrm.insert(make_pair("01011111", No)); //0x5F 

            one_byte_uses_modrm.insert(make_pair("01100000", No)); //0x60
            one_byte_uses_modrm.insert(make_pair("01100001", No)); //0x61
            one_byte_uses_modrm.insert(make_pair("01100010", Yes)); //0x62
            one_byte_uses_modrm.insert(make_pair("01100011", Yes)); //0x63
            one_byte_uses_modrm.insert(make_pair("01100100", No)); //0x64
            one_byte_uses_modrm.insert(make_pair("01100101", No)); //0x65
            one_byte_uses_modrm.insert(make_pair("01100110", No)); //0x66
            one_byte_uses_modrm.insert(make_pair("01100111", No)); //0x67
            one_byte_uses_modrm.insert(make_pair("01101000", No)); //0x68
            one_byte_uses_modrm.insert(make_pair("01101001", Yes)); //0x69
            one_byte_uses_modrm.insert(make_pair("01101010", No)); //0x6A
            one_byte_uses_modrm.insert(make_pair("01101011", Yes)); //0x6B
            one_byte_uses_modrm.insert(make_pair("01101100", No)); //0x6C
            one_byte_uses_modrm.insert(make_pair("01101101", No)); //0x6D
            one_byte_uses_modrm.insert(make_pair("01101110", No)); //0x6E
            one_byte_uses_modrm.insert(make_pair("01101111", No)); //0x6F 

            one_byte_uses_modrm.insert(make_pair("01110000", No)); //0x70
            one_byte_uses_modrm.insert(make_pair("01110001", No)); //0x71
            one_byte_uses_modrm.insert(make_pair("01110010", No)); //0x72
            one_byte_uses_modrm.insert(make_pair("01110011", No)); //0x73
            one_byte_uses_modrm.insert(make_pair("01110100", No)); //0x74
            one_byte_uses_modrm.insert(make_pair("01110101", No)); //0x75
            one_byte_uses_modrm.insert(make_pair("01110110", No)); //0x76
            one_byte_uses_modrm.insert(make_pair("01110111", No)); //0x77
            one_byte_uses_modrm.insert(make_pair("01111000", No)); //0x78
            one_byte_uses_modrm.insert(make_pair("01111001", No)); //0x79
            one_byte_uses_modrm.insert(make_pair("01111010", No)); //0x7A
            one_byte_uses_modrm.insert(make_pair("01111011", No)); //0x7B
            one_byte_uses_modrm.insert(make_pair("01111100", No)); //0x7C
            one_byte_uses_modrm.insert(make_pair("01111101", No)); //0x7D
            one_byte_uses_modrm.insert(make_pair("01111110", No)); //0x7E
            one_byte_uses_modrm.insert(make_pair("01111111", No)); //0x7F  

            one_byte_uses_modrm.insert(make_pair("10000000", Yes)); //0x80
            one_byte_uses_modrm.insert(make_pair("10000001", Yes)); //0x81
            one_byte_uses_modrm.insert(make_pair("10000010", Yes)); //0x82
            one_byte_uses_modrm.insert(make_pair("10000011", Yes)); //0x83
            one_byte_uses_modrm.insert(make_pair("10000100", Yes)); //0x84
            one_byte_uses_modrm.insert(make_pair("10000101", Yes)); //0x85
            one_byte_uses_modrm.insert(make_pair("10000110", Yes)); //0x86
            one_byte_uses_modrm.insert(make_pair("10000111", Yes)); //0x87
            one_byte_uses_modrm.insert(make_pair("10001000", Yes)); //0x88
            one_byte_uses_modrm.insert(make_pair("10001001", Yes)); //0x89
            one_byte_uses_modrm.insert(make_pair("10001010", Yes)); //0x8A
            one_byte_uses_modrm.insert(make_pair("10001011", Yes)); //0x8B
            one_byte_uses_modrm.insert(make_pair("10001100", Yes)); //0x8C
            one_byte_uses_modrm.insert(make_pair("10001101", Yes)); //0x8D
            one_byte_uses_modrm.insert(make_pair("10001110", Yes)); //0x8E
            one_byte_uses_modrm.insert(make_pair("10001111", Yes)); //0x8F 

            one_byte_uses_modrm.insert(make_pair("10010000", No)); //0x90
            one_byte_uses_modrm.insert(make_pair("10010001", No)); //0x91
            one_byte_uses_modrm.insert(make_pair("10010010", No)); //0x92
            one_byte_uses_modrm.insert(make_pair("10010011", No)); //0x93
            one_byte_uses_modrm.insert(make_pair("10010100", No)); //0x94
            one_byte_uses_modrm.insert(make_pair("10010101", No)); //0x95
            one_byte_uses_modrm.insert(make_pair("10010110", No)); //0x96
            one_byte_uses_modrm.insert(make_pair("10010111", No)); //0x97
            one_byte_uses_modrm.insert(make_pair("10011000", No)); //0x98
            one_byte_uses_modrm.insert(make_pair("10011001", No)); //0x99
            one_byte_uses_modrm.insert(make_pair("10011010", No)); //0x9A
            one_byte_uses_modrm.insert(make_pair("10011011", No)); //0x9B
            one_byte_uses_modrm.insert(make_pair("10011100", No)); //0x9C
            one_byte_uses_modrm.insert(make_pair("10011101", No)); //0x9D
            one_byte_uses_modrm.insert(make_pair("10011110", No)); //0x9E
            one_byte_uses_modrm.insert(make_pair("10011111", No)); //0x9F 

            one_byte_uses_modrm.insert(make_pair("10100000", No)); //0xA0
            one_byte_uses_modrm.insert(make_pair("10100001", No)); //0xA1
            one_byte_uses_modrm.insert(make_pair("10100010", No)); //0xA2
            one_byte_uses_modrm.insert(make_pair("10100011", No)); //0xA3
            one_byte_uses_modrm.insert(make_pair("10100100", No)); //0xA4
            one_byte_uses_modrm.insert(make_pair("10100101", No)); //0xA5
            one_byte_uses_modrm.insert(make_pair("10100110", No)); //0xA6
            one_byte_uses_modrm.insert(make_pair("10100111", No)); //0xA7
            one_byte_uses_modrm.insert(make_pair("10101000", No)); //0xA8
            one_byte_uses_modrm.insert(make_pair("10101001", No)); //0xA9
            one_byte_uses_modrm.insert(make_pair("10101010", No)); //0xAA
            one_byte_uses_modrm.insert(make_pair("10101011", No)); //0xAB
            one_byte_uses_modrm.insert(make_pair("10101100", No)); //0xAC
            one_byte_uses_modrm.insert(make_pair("10101101", No)); //0xAD
            one_byte_uses_modrm.insert(make_pair("10101110", No)); //0xAE
            one_byte_uses_modrm.insert(make_pair("10101111", No)); //0xAF 

            one_byte_uses_modrm.insert(make_pair("10110000", No)); //0xB0
            one_byte_uses_modrm.insert(make_pair("10110001", No)); //0xB1
            one_byte_uses_modrm.insert(make_pair("10110010", No)); //0xB2
            one_byte_uses_modrm.insert(make_pair("10110011", No)); //0xB3
            one_byte_uses_modrm.insert(make_pair("10110100", No)); //0xB4
            one_byte_uses_modrm.insert(make_pair("10110101", No)); //0xB5
            one_byte_uses_modrm.insert(make_pair("10110110", No)); //0xB6
            one_byte_uses_modrm.insert(make_pair("10110111", No)); //0xB7
            one_byte_uses_modrm.insert(make_pair("10111000", No)); //0xB8
            one_byte_uses_modrm.insert(make_pair("10111001", No)); //0xB9
            one_byte_uses_modrm.insert(make_pair("10111010", No)); //0xBA
            one_byte_uses_modrm.insert(make_pair("10111011", No)); //0xBB
            one_byte_uses_modrm.insert(make_pair("10111100", No)); //0xBC
            one_byte_uses_modrm.insert(make_pair("10111101", No)); //0xBD
            one_byte_uses_modrm.insert(make_pair("10111110", No)); //0xBE
            one_byte_uses_modrm.insert(make_pair("10111111", No)); //0xBF 

            one_byte_uses_modrm.insert(make_pair("11000000", Yes)); //0xC0
            one_byte_uses_modrm.insert(make_pair("11000001", Yes)); //0xC1
            one_byte_uses_modrm.insert(make_pair("11000010", No)); //0xC2
            one_byte_uses_modrm.insert(make_pair("11000011", No)); //0xC3
            one_byte_uses_modrm.insert(make_pair("11000100", Yes)); //0xC4
            one_byte_uses_modrm.insert(make_pair("11000101", Yes)); //0xC5
            one_byte_uses_modrm.insert(make_pair("11000110", Yes)); //0xC6
            one_byte_uses_modrm.insert(make_pair("11000111", Yes)); //0xC7
            one_byte_uses_modrm.insert(make_pair("11001000", No)); //0xC8
            one_byte_uses_modrm.insert(make_pair("11001001", No)); //0xC9
            one_byte_uses_modrm.insert(make_pair("11001010", No)); //0xCA
            one_byte_uses_modrm.insert(make_pair("11001011", No)); //0xCB
            one_byte_uses_modrm.insert(make_pair("11001100", No)); //0xCC
            one_byte_uses_modrm.insert(make_pair("11001101", No)); //0xCD
            one_byte_uses_modrm.insert(make_pair("11001110", No)); //0xCE
            one_byte_uses_modrm.insert(make_pair("11001111", No)); //0xCF 

            one_byte_uses_modrm.insert(make_pair("11010000", Yes)); //0xD0
            one_byte_uses_modrm.insert(make_pair("11010001", Yes)); //0xD1
            one_byte_uses_modrm.insert(make_pair("11010010", Yes)); //0xD2
            one_byte_uses_modrm.insert(make_pair("11010011", Yes)); //0xD3
            one_byte_uses_modrm.insert(make_pair("11010100", No)); //0xD4
            one_byte_uses_modrm.insert(make_pair("11010101", No)); //0xD5
            one_byte_uses_modrm.insert(make_pair("11010110", No)); //0xD6
            one_byte_uses_modrm.insert(make_pair("11010111", No)); //0xD7
            one_byte_uses_modrm.insert(make_pair("11011000", Yes)); //0xD8
            one_byte_uses_modrm.insert(make_pair("11011001", Yes)); //0xD9
            one_byte_uses_modrm.insert(make_pair("11011010", Yes)); //0xDA
            one_byte_uses_modrm.insert(make_pair("11011011", Yes)); //0xDB
            one_byte_uses_modrm.insert(make_pair("11011100", Yes)); //0xDC
            one_byte_uses_modrm.insert(make_pair("11011101", Yes)); //0xDD
            one_byte_uses_modrm.insert(make_pair("11011110", Yes)); //0xDE
            one_byte_uses_modrm.insert(make_pair("11011111", Yes)); //0xDF 

            one_byte_uses_modrm.insert(make_pair("11100000", No)); //0xE0
            one_byte_uses_modrm.insert(make_pair("11100001", No)); //0xE1
            one_byte_uses_modrm.insert(make_pair("11100010", No)); //0xE2
            one_byte_uses_modrm.insert(make_pair("11100011", No)); //0xE3
            one_byte_uses_modrm.insert(make_pair("11100100", No)); //0xE4
            one_byte_uses_modrm.insert(make_pair("11100101", No)); //0xE5
            one_byte_uses_modrm.insert(make_pair("11100110", No)); //0xE6
            one_byte_uses_modrm.insert(make_pair("11100111", No)); //0xE7
            one_byte_uses_modrm.insert(make_pair("11101000", No)); //0xE8
            one_byte_uses_modrm.insert(make_pair("11101001", No)); //0xE9
            one_byte_uses_modrm.insert(make_pair("11101010", No)); //0xEA
            one_byte_uses_modrm.insert(make_pair("11101011", No)); //0xEB
            one_byte_uses_modrm.insert(make_pair("11101100", No)); //0xEC
            one_byte_uses_modrm.insert(make_pair("11101101", No)); //0xED
            one_byte_uses_modrm.insert(make_pair("11101110", No)); //0xEE
            one_byte_uses_modrm.insert(make_pair("11101111", No)); //0xEF 

            one_byte_uses_modrm.insert(make_pair("11110000", No)); //0xF0
            one_byte_uses_modrm.insert(make_pair("11110001", No)); //0xF1
            one_byte_uses_modrm.insert(make_pair("11110010", No)); //0xF2
            one_byte_uses_modrm.insert(make_pair("11110011", No)); //0xF3
            one_byte_uses_modrm.insert(make_pair("11110100", No)); //0xF4
            one_byte_uses_modrm.insert(make_pair("11110101", No)); //0xF5
            one_byte_uses_modrm.insert(make_pair("11110110", Yes)); //0xF6
            one_byte_uses_modrm.insert(make_pair("11110111", Yes)); //0xF7
            one_byte_uses_modrm.insert(make_pair("11111000", No)); //0xF8
            one_byte_uses_modrm.insert(make_pair("11111001", No)); //0xF9
            one_byte_uses_modrm.insert(make_pair("11111010", No)); //0xFA
            one_byte_uses_modrm.insert(make_pair("11111011", No)); //0xFB
            one_byte_uses_modrm.insert(make_pair("11111100", No)); //0xFC
            one_byte_uses_modrm.insert(make_pair("11111101", No)); //0xFD
            one_byte_uses_modrm.insert(make_pair("11111110", Yes)); //0xFE
            one_byte_uses_modrm.insert(make_pair("11111111", Yes)); //0xFF         

            // two byte modrm
            two_byte_uses_modrm.insert(make_pair("00000000", Yes)); //0x00
            two_byte_uses_modrm.insert(make_pair("00000001", Yes)); //0x01
            two_byte_uses_modrm.insert(make_pair("00000010", Yes)); //0x02
            two_byte_uses_modrm.insert(make_pair("00000011", Yes)); //0x03
            two_byte_uses_modrm.insert(make_pair("00000100", No)); //0x04
            two_byte_uses_modrm.insert(make_pair("00000101", No)); //0x05
            two_byte_uses_modrm.insert(make_pair("00000110", No)); //0x06
            two_byte_uses_modrm.insert(make_pair("00000111", No)); //0x07
            two_byte_uses_modrm.insert(make_pair("00001000", No)); //0x08
            two_byte_uses_modrm.insert(make_pair("00001001", No)); //0x09
            two_byte_uses_modrm.insert(make_pair("00001010", No)); //0x0A
            two_byte_uses_modrm.insert(make_pair("00001011", No)); //0x0B
            two_byte_uses_modrm.insert(make_pair("00001100", No)); //0x0C
            two_byte_uses_modrm.insert(make_pair("00001101", Yes)); //0x0D
            two_byte_uses_modrm.insert(make_pair("00001110", No)); //0x0E
            two_byte_uses_modrm.insert(make_pair("00001111", Yes)); //0x0F 

            two_byte_uses_modrm.insert(make_pair("00010000", Yes)); //0x10
            two_byte_uses_modrm.insert(make_pair("00010001", Yes)); //0x11
            two_byte_uses_modrm.insert(make_pair("00010010", Yes)); //0x12
            two_byte_uses_modrm.insert(make_pair("00010011", Yes)); //0x13
            two_byte_uses_modrm.insert(make_pair("00010100", Yes)); //0x14
            two_byte_uses_modrm.insert(make_pair("00010101", Yes)); //0x15
            two_byte_uses_modrm.insert(make_pair("00010110", Yes)); //0x16
            two_byte_uses_modrm.insert(make_pair("00010111", Yes)); //0x17
            two_byte_uses_modrm.insert(make_pair("00011000", Yes)); //0x18
            two_byte_uses_modrm.insert(make_pair("00011001", Yes)); //0x19
            two_byte_uses_modrm.insert(make_pair("00011010", Yes)); //0x1A
            two_byte_uses_modrm.insert(make_pair("00011011", Yes)); //0x1B
            two_byte_uses_modrm.insert(make_pair("00011100", Yes)); //0x1C
            two_byte_uses_modrm.insert(make_pair("00011101", Yes)); //0x1D
            two_byte_uses_modrm.insert(make_pair("00011110", Yes)); //0x1E
            two_byte_uses_modrm.insert(make_pair("00011111", Yes)); //0x1F 

            two_byte_uses_modrm.insert(make_pair("00100000", Yes)); //0x20
            two_byte_uses_modrm.insert(make_pair("00100001", Yes)); //0x21
            two_byte_uses_modrm.insert(make_pair("00100010", Yes)); //0x22
            two_byte_uses_modrm.insert(make_pair("00100011", Yes)); //0x23
            two_byte_uses_modrm.insert(make_pair("00100100", Yes)); //0x24
            two_byte_uses_modrm.insert(make_pair("00100101", No)); //0x25
            two_byte_uses_modrm.insert(make_pair("00100110", Yes)); //0x26
            two_byte_uses_modrm.insert(make_pair("00100111", No)); //0x27
            two_byte_uses_modrm.insert(make_pair("00101000", Yes)); //0x28
            two_byte_uses_modrm.insert(make_pair("00101001", Yes)); //0x29
            two_byte_uses_modrm.insert(make_pair("00101010", Yes)); //0x2A
            two_byte_uses_modrm.insert(make_pair("00101011", Yes)); //0x2B
            two_byte_uses_modrm.insert(make_pair("00101100", Yes)); //0x2C
            two_byte_uses_modrm.insert(make_pair("00101101", Yes)); //0x2D
            two_byte_uses_modrm.insert(make_pair("00101110", Yes)); //0x2E
            two_byte_uses_modrm.insert(make_pair("00101111", Yes)); //0x2F 

            two_byte_uses_modrm.insert(make_pair("00110000", No)); //0x30
            two_byte_uses_modrm.insert(make_pair("00110001", No)); //0x31
            two_byte_uses_modrm.insert(make_pair("00110010", No)); //0x32
            two_byte_uses_modrm.insert(make_pair("00110011", No)); //0x33
            two_byte_uses_modrm.insert(make_pair("00110100", No)); //0x34
            two_byte_uses_modrm.insert(make_pair("00110101", No)); //0x35
            two_byte_uses_modrm.insert(make_pair("00110110", No)); //0x36
            two_byte_uses_modrm.insert(make_pair("00110111", No)); //0x37
            two_byte_uses_modrm.insert(make_pair("00111000", No)); //0x38
            two_byte_uses_modrm.insert(make_pair("00111001", No)); //0x39
            two_byte_uses_modrm.insert(make_pair("00111010", No)); //0x3A
            two_byte_uses_modrm.insert(make_pair("00111011", No)); //0x3B
            two_byte_uses_modrm.insert(make_pair("00111100", No)); //0x3C
            two_byte_uses_modrm.insert(make_pair("00111101", No)); //0x3D
            two_byte_uses_modrm.insert(make_pair("00111110", No)); //0x3E
            two_byte_uses_modrm.insert(make_pair("00111111", No)); //0x3F 

            two_byte_uses_modrm.insert(make_pair("01000000", Yes)); //0x40
            two_byte_uses_modrm.insert(make_pair("01000001", Yes)); //0x41
            two_byte_uses_modrm.insert(make_pair("01000010", Yes)); //0x42
            two_byte_uses_modrm.insert(make_pair("01000011", Yes)); //0x43
            two_byte_uses_modrm.insert(make_pair("01000100", Yes)); //0x44
            two_byte_uses_modrm.insert(make_pair("01000101", Yes)); //0x45
            two_byte_uses_modrm.insert(make_pair("01000110", Yes)); //0x46
            two_byte_uses_modrm.insert(make_pair("01000111", Yes)); //0x47
            two_byte_uses_modrm.insert(make_pair("01001000", Yes)); //0x48
            two_byte_uses_modrm.insert(make_pair("01001001", Yes)); //0x49
            two_byte_uses_modrm.insert(make_pair("01001010", Yes)); //0x4A
            two_byte_uses_modrm.insert(make_pair("01001011", Yes)); //0x4B
            two_byte_uses_modrm.insert(make_pair("01001100", Yes)); //0x4C
            two_byte_uses_modrm.insert(make_pair("01001101", Yes)); //0x4D
            two_byte_uses_modrm.insert(make_pair("01001110", Yes)); //0x4E
            two_byte_uses_modrm.insert(make_pair("01001111", Yes)); //0x4F 

            two_byte_uses_modrm.insert(make_pair("01010000", Yes)); //0x50
            two_byte_uses_modrm.insert(make_pair("01010001", Yes)); //0x51
            two_byte_uses_modrm.insert(make_pair("01010010", Yes)); //0x52
            two_byte_uses_modrm.insert(make_pair("01010011", Yes)); //0x53
            two_byte_uses_modrm.insert(make_pair("01010100", Yes)); //0x54
            two_byte_uses_modrm.insert(make_pair("01010101", Yes)); //0x55
            two_byte_uses_modrm.insert(make_pair("01010110", Yes)); //0x56
            two_byte_uses_modrm.insert(make_pair("01010111", Yes)); //0x57
            two_byte_uses_modrm.insert(make_pair("01011000", Yes)); //0x58
            two_byte_uses_modrm.insert(make_pair("01011001", Yes)); //0x59
            two_byte_uses_modrm.insert(make_pair("01011010", Yes)); //0x5A
            two_byte_uses_modrm.insert(make_pair("01011011", Yes)); //0x5B
            two_byte_uses_modrm.insert(make_pair("01011100", Yes)); //0x5C
            two_byte_uses_modrm.insert(make_pair("01011101", Yes)); //0x5D
            two_byte_uses_modrm.insert(make_pair("01011110", Yes)); //0x5E
            two_byte_uses_modrm.insert(make_pair("01011111", Yes)); //0x5F 

            two_byte_uses_modrm.insert(make_pair("01100000", Yes)); //0x60
            two_byte_uses_modrm.insert(make_pair("01100001", Yes)); //0x61
            two_byte_uses_modrm.insert(make_pair("01100010", Yes)); //0x62
            two_byte_uses_modrm.insert(make_pair("01100011", Yes)); //0x63
            two_byte_uses_modrm.insert(make_pair("01100100", Yes)); //0x64
            two_byte_uses_modrm.insert(make_pair("01100101", Yes)); //0x65
            two_byte_uses_modrm.insert(make_pair("01100110", Yes)); //0x66
            two_byte_uses_modrm.insert(make_pair("01100111", Yes)); //0x67
            two_byte_uses_modrm.insert(make_pair("01101000", Yes)); //0x68
            two_byte_uses_modrm.insert(make_pair("01101001", Yes)); //0x69
            two_byte_uses_modrm.insert(make_pair("01101010", Yes)); //0x6A
            two_byte_uses_modrm.insert(make_pair("01101011", Yes)); //0x6B
            two_byte_uses_modrm.insert(make_pair("01101100", Yes)); //0x6C
            two_byte_uses_modrm.insert(make_pair("01101101", Yes)); //0x6D
            two_byte_uses_modrm.insert(make_pair("01101110", Yes)); //0x6E
            two_byte_uses_modrm.insert(make_pair("01101111", Yes)); //0x6F 

            two_byte_uses_modrm.insert(make_pair("01110000", Yes)); //0x70
            two_byte_uses_modrm.insert(make_pair("01110001", Yes)); //0x71
            two_byte_uses_modrm.insert(make_pair("01110010", Yes)); //0x72
            two_byte_uses_modrm.insert(make_pair("01110011", Yes)); //0x73
            two_byte_uses_modrm.insert(make_pair("01110100", Yes)); //0x74
            two_byte_uses_modrm.insert(make_pair("01110101", Yes)); //0x75
            two_byte_uses_modrm.insert(make_pair("01110110", Yes)); //0x76
            two_byte_uses_modrm.insert(make_pair("01110111", No)); //0x77
            two_byte_uses_modrm.insert(make_pair("01111000", No)); //0x78
            two_byte_uses_modrm.insert(make_pair("01111001", No)); //0x79
            two_byte_uses_modrm.insert(make_pair("01111010", No)); //0x7A
            two_byte_uses_modrm.insert(make_pair("01111011", No)); //0x7B
            two_byte_uses_modrm.insert(make_pair("01111100", Yes)); //0x7C
            two_byte_uses_modrm.insert(make_pair("01111101", Yes)); //0x7D
            two_byte_uses_modrm.insert(make_pair("01111110", Yes)); //0x7E
            two_byte_uses_modrm.insert(make_pair("01111111", Yes)); //0x7F  

            two_byte_uses_modrm.insert(make_pair("10000000", No)); //0x80
            two_byte_uses_modrm.insert(make_pair("10000001", No)); //0x81
            two_byte_uses_modrm.insert(make_pair("10000010", No)); //0x82
            two_byte_uses_modrm.insert(make_pair("10000011", No)); //0x83
            two_byte_uses_modrm.insert(make_pair("10000100", No)); //0x84
            two_byte_uses_modrm.insert(make_pair("10000101", No)); //0x85
            two_byte_uses_modrm.insert(make_pair("10000110", No)); //0x86
            two_byte_uses_modrm.insert(make_pair("10000111", No)); //0x87
            two_byte_uses_modrm.insert(make_pair("10001000", No)); //0x88
            two_byte_uses_modrm.insert(make_pair("10001001", No)); //0x89
            two_byte_uses_modrm.insert(make_pair("10001010", No)); //0x8A
            two_byte_uses_modrm.insert(make_pair("10001011", No)); //0x8B
            two_byte_uses_modrm.insert(make_pair("10001100", No)); //0x8C
            two_byte_uses_modrm.insert(make_pair("10001101", No)); //0x8D
            two_byte_uses_modrm.insert(make_pair("10001110", No)); //0x8E
            two_byte_uses_modrm.insert(make_pair("10001111", No)); //0x8F 

            two_byte_uses_modrm.insert(make_pair("10010000", Yes)); //0x90
            two_byte_uses_modrm.insert(make_pair("10010001", Yes)); //0x91
            two_byte_uses_modrm.insert(make_pair("10010010", Yes)); //0x92
            two_byte_uses_modrm.insert(make_pair("10010011", Yes)); //0x93
            two_byte_uses_modrm.insert(make_pair("10010100", Yes)); //0x94
            two_byte_uses_modrm.insert(make_pair("10010101", Yes)); //0x95
            two_byte_uses_modrm.insert(make_pair("10010110", Yes)); //0x96
            two_byte_uses_modrm.insert(make_pair("10010111", Yes)); //0x97
            two_byte_uses_modrm.insert(make_pair("10011000", Yes)); //0x98
            two_byte_uses_modrm.insert(make_pair("10011001", Yes)); //0x99
            two_byte_uses_modrm.insert(make_pair("10011010", Yes)); //0x9A
            two_byte_uses_modrm.insert(make_pair("10011011", Yes)); //0x9B
            two_byte_uses_modrm.insert(make_pair("10011100", Yes)); //0x9C
            two_byte_uses_modrm.insert(make_pair("10011101", Yes)); //0x9D
            two_byte_uses_modrm.insert(make_pair("10011110", Yes)); //0x9E
            two_byte_uses_modrm.insert(make_pair("10011111", Yes)); //0x9F 

            two_byte_uses_modrm.insert(make_pair("10100000", No)); //0xA0
            two_byte_uses_modrm.insert(make_pair("10100001", No)); //0xA1
            two_byte_uses_modrm.insert(make_pair("10100010", No)); //0xA2
            two_byte_uses_modrm.insert(make_pair("10100011", Yes)); //0xA3
            two_byte_uses_modrm.insert(make_pair("10100100", Yes)); //0xA4
            two_byte_uses_modrm.insert(make_pair("10100101", Yes)); //0xA5
            two_byte_uses_modrm.insert(make_pair("10100110", Yes)); //0xA6
            two_byte_uses_modrm.insert(make_pair("10100111", Yes)); //0xA7
            two_byte_uses_modrm.insert(make_pair("10101000", No)); //0xA8
            two_byte_uses_modrm.insert(make_pair("10101001", No)); //0xA9
            two_byte_uses_modrm.insert(make_pair("10101010", No)); //0xAA
            two_byte_uses_modrm.insert(make_pair("10101011", Yes)); //0xAB
            two_byte_uses_modrm.insert(make_pair("10101100", Yes)); //0xAC
            two_byte_uses_modrm.insert(make_pair("10101101", Yes)); //0xAD
            two_byte_uses_modrm.insert(make_pair("10101110", Yes)); //0xAE
            two_byte_uses_modrm.insert(make_pair("10101111", Yes)); //0xAF 

            two_byte_uses_modrm.insert(make_pair("10110000", Yes)); //0xB0
            two_byte_uses_modrm.insert(make_pair("10110001", Yes)); //0xB1
            two_byte_uses_modrm.insert(make_pair("10110010", Yes)); //0xB2
            two_byte_uses_modrm.insert(make_pair("10110011", Yes)); //0xB3
            two_byte_uses_modrm.insert(make_pair("10110100", Yes)); //0xB4
            two_byte_uses_modrm.insert(make_pair("10110101", Yes)); //0xB5
            two_byte_uses_modrm.insert(make_pair("10110110", Yes)); //0xB6
            two_byte_uses_modrm.insert(make_pair("10110111", Yes)); //0xB7
            two_byte_uses_modrm.insert(make_pair("10111000", No)); //0xB8
            two_byte_uses_modrm.insert(make_pair("10111001", No)); //0xB9
            two_byte_uses_modrm.insert(make_pair("10111010", Yes)); //0xBA
            two_byte_uses_modrm.insert(make_pair("10111011", Yes)); //0xBB
            two_byte_uses_modrm.insert(make_pair("10111100", Yes)); //0xBC
            two_byte_uses_modrm.insert(make_pair("10111101", Yes)); //0xBD
            two_byte_uses_modrm.insert(make_pair("10111110", Yes)); //0xBE
            two_byte_uses_modrm.insert(make_pair("10111111", Yes)); //0xBF 

            two_byte_uses_modrm.insert(make_pair("11000000", Yes)); //0xC0
            two_byte_uses_modrm.insert(make_pair("11000001", Yes)); //0xC1
            two_byte_uses_modrm.insert(make_pair("11000010", Yes)); //0xC2
            two_byte_uses_modrm.insert(make_pair("11000011", Yes)); //0xC3
            two_byte_uses_modrm.insert(make_pair("11000100", Yes)); //0xC4
            two_byte_uses_modrm.insert(make_pair("11000101", Yes)); //0xC5
            two_byte_uses_modrm.insert(make_pair("11000110", Yes)); //0xC6
            two_byte_uses_modrm.insert(make_pair("11000111", Yes)); //0xC7
            two_byte_uses_modrm.insert(make_pair("11001000", No)); //0xC8
            two_byte_uses_modrm.insert(make_pair("11001001", No)); //0xC9
            two_byte_uses_modrm.insert(make_pair("11001010", No)); //0xCA
            two_byte_uses_modrm.insert(make_pair("11001011", No)); //0xCB
            two_byte_uses_modrm.insert(make_pair("11001100", No)); //0xCC
            two_byte_uses_modrm.insert(make_pair("11001101", No)); //0xCD
            two_byte_uses_modrm.insert(make_pair("11001110", No)); //0xCE
            two_byte_uses_modrm.insert(make_pair("11001111", No)); //0xCF 

            two_byte_uses_modrm.insert(make_pair("11010000", Yes)); //0xD0
            two_byte_uses_modrm.insert(make_pair("11010001", Yes)); //0xD1
            two_byte_uses_modrm.insert(make_pair("11010010", Yes)); //0xD2
            two_byte_uses_modrm.insert(make_pair("11010011", Yes)); //0xD3
            two_byte_uses_modrm.insert(make_pair("11010100", Yes)); //0xD4
            two_byte_uses_modrm.insert(make_pair("11010101", Yes)); //0xD5
            two_byte_uses_modrm.insert(make_pair("11010110", Yes)); //0xD6
            two_byte_uses_modrm.insert(make_pair("11010111", Yes)); //0xD7
            two_byte_uses_modrm.insert(make_pair("11011000", Yes)); //0xD8
            two_byte_uses_modrm.insert(make_pair("11011001", Yes)); //0xD9
            two_byte_uses_modrm.insert(make_pair("11011010", Yes)); //0xDA
            two_byte_uses_modrm.insert(make_pair("11011011", Yes)); //0xDB
            two_byte_uses_modrm.insert(make_pair("11011100", Yes)); //0xDC
            two_byte_uses_modrm.insert(make_pair("11011101", Yes)); //0xDD
            two_byte_uses_modrm.insert(make_pair("11011110", Yes)); //0xDE
            two_byte_uses_modrm.insert(make_pair("11011111", Yes)); //0xDF 

            two_byte_uses_modrm.insert(make_pair("11100000", Yes)); //0xE0
            two_byte_uses_modrm.insert(make_pair("11100001", Yes)); //0xE1
            two_byte_uses_modrm.insert(make_pair("11100010", Yes)); //0xE2
            two_byte_uses_modrm.insert(make_pair("11100011", Yes)); //0xE3
            two_byte_uses_modrm.insert(make_pair("11100100", Yes)); //0xE4
            two_byte_uses_modrm.insert(make_pair("11100101", Yes)); //0xE5
            two_byte_uses_modrm.insert(make_pair("11100110", Yes)); //0xE6
            two_byte_uses_modrm.insert(make_pair("11100111", Yes)); //0xE7
            two_byte_uses_modrm.insert(make_pair("11101000", Yes)); //0xE8
            two_byte_uses_modrm.insert(make_pair("11101001", Yes)); //0xE9
            two_byte_uses_modrm.insert(make_pair("11101010", Yes)); //0xEA
            two_byte_uses_modrm.insert(make_pair("11101011", Yes)); //0xEB
            two_byte_uses_modrm.insert(make_pair("11101100", Yes)); //0xEC
            two_byte_uses_modrm.insert(make_pair("11101101", Yes)); //0xED
            two_byte_uses_modrm.insert(make_pair("11101110", Yes)); //0xEE
            two_byte_uses_modrm.insert(make_pair("11101111", Yes)); //0xEF 

            two_byte_uses_modrm.insert(make_pair("11110000", Yes)); //0xF0
            two_byte_uses_modrm.insert(make_pair("11110001", Yes)); //0xF1
            two_byte_uses_modrm.insert(make_pair("11110010", Yes)); //0xF2
            two_byte_uses_modrm.insert(make_pair("11110011", Yes)); //0xF3
            two_byte_uses_modrm.insert(make_pair("11110100", Yes)); //0xF4
            two_byte_uses_modrm.insert(make_pair("11110101", Yes)); //0xF5
            two_byte_uses_modrm.insert(make_pair("11110110", Yes)); //0xF6
            two_byte_uses_modrm.insert(make_pair("11110111", Yes)); //0xF7
            two_byte_uses_modrm.insert(make_pair("11111000", Yes)); //0xF8
            two_byte_uses_modrm.insert(make_pair("11111001", Yes)); //0xF9
            two_byte_uses_modrm.insert(make_pair("11111010", Yes)); //0xFA
            two_byte_uses_modrm.insert(make_pair("11111011", Yes)); //0xFB
            two_byte_uses_modrm.insert(make_pair("11111100", Yes)); //0xFC
            two_byte_uses_modrm.insert(make_pair("11111101", Yes)); //0xFD
            two_byte_uses_modrm.insert(make_pair("11111110", Yes)); //0xFE
            two_byte_uses_modrm.insert(make_pair("11111111", No)); //0xFF     	

            register_map_file.insert(make_pair("0000", 0));
            register_map_file.insert(make_pair("0001", 0));        
            register_map_file.insert(make_pair("0010", 0));
            register_map_file.insert(make_pair("0011", 0));
            register_map_file.insert(make_pair("0100", 67107840));
            register_map_file.insert(make_pair("0101", 0));        
            register_map_file.insert(make_pair("0110", 0));
            register_map_file.insert(make_pair("0111", 0));
            register_map_file.insert(make_pair("1000", 0));
            register_map_file.insert(make_pair("1001", 0));        
            register_map_file.insert(make_pair("1010", 0));
            register_map_file.insert(make_pair("1011", 0));
            register_map_file.insert(make_pair("1100", 0));
            register_map_file.insert(make_pair("1101", 0));        
            register_map_file.insert(make_pair("1110", 0));
            register_map_file.insert(make_pair("1111", 0));

	    flag.carry_flag = false;
	    flag.parity_flag = false;
	    flag.zero_flag = false;
	    flag.sign_flag = false;
	    flag.direction_flag = false;
	    flag.overflow_flag = false;

	    dcopy.valid = false;
            state = PrefixState;
	    dcache_read_request = false;
	    cache_read_done = false; 
        }
};

#endif
