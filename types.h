/*
 * Copyright (c) 2007 The Hewlett-Packard Development Company
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Gabe Black
 */
 
#include <string> 
#include "systemc.h"

using namespace std;

struct Legacy
{
	bool decodeVal;
	bool repne;
	bool rep;
	bool lock;
	bool op;
	bool addr;
	//There can be only one segment override, so they share the
	//first 3 bits in the legacyPrefixes bitfield.
	bool seg;	
};

struct Rex
{
	char w;
	char r;
	char x;
	char b;	
};

struct ModRM
{
    std::string mod; //Bit 7 - 6
    std::string reg; //Bit 5 - 3
    std::string rm; //Bit 2 - 0
};

struct Sib
{
    std::string scale; //Bit 7 - 6
    std::string index; //Bit 5 - 3
    std::string base; //Bit 2 - 0
};

struct Instruction
{
	uint64_t instruction_address;
    uint64_t next_instruction_address;
	struct Legacy legacy;
    //Prefixes
    struct Rex rex;
    //This holds all of the bytes of the opcode
    struct
    {
        //The number of bytes in this opcode. Right now, we ignore that
        //this can be 3 in some cases
        uint8_t num;
        //The first byte detected in a 2+ byte opcode. Should be 0xF0.
        std::string prefixA;
        //The second byte detected in a 3+ byte opcode. Could be 0x38-0x3F
        //for some SSE instructions. 3dNow! instructions are handled as
        //two byte opcodes and then split out further by the immediate
        //byte.
        std::string prefixB;
        //The main opcode byte. The highest addressed byte in the opcode.
        std::string op;
    } opcode;
    //Modifier bytes
    //ModRM modRM;
    //Sib sib;
    //Immediate fields
    uint64_t immediate;
    uint64_t displacement;

    //The effective operand size.
    uint8_t opSize;
    //The effective address size.
    uint8_t addrSize;
    //The effective stack size.
    uint8_t stackSize;
    //The size of the displacement
    uint8_t dispSize;

    ModRM modRM;
    Sib sib;
    bool immed_flag;

    //Mode information
    //OperatingMode mode;
};

struct execution_struct {
	uint8_t rex_byte;
	uint64_t address;
	uint64_t immediate;
	std::string rm_register;
	std::string reg_register;
	uint8_t opcode;
	uint8_t sub_op;
	uint8_t direction;
	uint64_t curr_instr_address;
	uint64_t rip;
	bool r2rmode;
};

struct control_flags {
	bool carry_flag;
	bool parity_flag;
	bool zero_flag;
	bool sign_flag;
	bool direction_flag;
	bool overflow_flag;
};

struct dcache_copy {
	uint64_t address;
	uint64_t data;
	bool valid;
};
