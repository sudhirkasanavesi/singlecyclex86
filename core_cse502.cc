/*
 * Copyright (c) 2011 Google
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Gabe Black
 */

#include <iostream>
#include <bitset>
#include <stack>
#include <queue>
#include <ostream>
using namespace std;

//#define SUDHIR
//#define VINAY
//#define PROG1 0 
//#define PROG2 2
#define VIDHYA
#include "core_cse502.h"
template <class Container, class Stream>
Stream& printOneValueContainer
    (Stream& outputstream, const Container& container)
{
    typename Container::const_iterator beg = container.begin();

    outputstream << "[";

    while(beg != container.end())
    {
        outputstream << " " << *beg++;
    }

    outputstream << " ]";

    return outputstream;
}

template < class Type, class Container >
const Container& container
    (const std::stack<Type, Container>& stack)
{
    struct HackedStack : private std::stack<Type, Container>
    {
        static const Container& container
            (const std::stack<Type, Container>& stack)
        {
            return stack.*&HackedStack::c;
        }
    };

    return HackedStack::container(stack);
}

template < class Type, class Container >
const Container& container
    (const std::queue<Type, Container>& queue)
{
    struct HackedQueue : private std::queue<Type, Container>
    {
        static const Container& container
            (const std::queue<Type, Container>& queue)
        {
            return queue.*&HackedQueue::c;
        }
    };

    return HackedQueue::container(queue);
}

template
    < class Type
    , template <class Type, class Container = std::deque<Type> > class Adapter
    , class Stream
    >
Stream& operator<<
    (Stream& outputstream, const Adapter<Type>& adapter)
{
    return printOneValueContainer(outputstream, container(adapter));
}
stack<uint64_t> debug_stack;
bool first_run_for_decode = true;
uint64_t return_address;
string register_string[] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", 
			    "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};
int bin2dec(string binary) {
        int decimal = 0;

        for(int counter = binary.size()-1; counter >= 0; counter--)
                if(binary.c_str()[counter] == '1') {
                        decimal += pow(2, binary.size()-counter-1);
                }

        return decimal;
}

void init_exec_struct(struct execution_struct &exc) {
	exc.rex_byte = 0;
	exc.address = 0;
	exc.immediate = 0;
	exc.rm_register = "";
	exc.reg_register = "";
	exc.opcode = 0;
	exc.sub_op = 10;
	exc.direction = 0;
	exc.r2rmode = false;
}

const uint8_t CoreCSE502::SizeTypeToSize[3][10] =
{
//  noimm byte word dword qword oword vword zword enter pointer
    {0,    1,   2,   4,    8,    16,   2,    2,    3,    4      }, //16 bit
    {0,    1,   2,   4,    8,    16,   4,    4,    3,    6      }, //32 bit
    {0,    1,   2,   4,    8,    16,   8,    4,    3,    0      }  //64 bit
};

/**
 * Generate a 64-bit mask of 'nbits' 1s, right justified.
 */
inline uint64_t mask(int nbits)
{
    return (nbits == 64) ? (uint64_t)-1LL : (1ULL << nbits) - 1;
}



/**
 * Extract the bitfield from position 'first' to 'last' (inclusive)
 * from 'val' and right justify it.  MSB is numbered 63, LSB is 0.
 */
template <class T>
inline T bits(T val, int first, int last)
{
    int nbits = first - last + 1;
    return (val >> last) & mask(nbits);
}


template <int N>
inline int64_t sext(uint64_t val)
{
    int sign_bit = bits(val, N-1, N-1);
    return sign_bit ? (val | ~mask(N)) : val;
}

void CoreCSE502::do_reset() {
	Core::do_reset();
}

bool CoreCSE502::fetch()
{
#if 0
	cout<<"**************************************"<<endl;
	cout<<"RIP "<<RIP<<endl;
	cout<<"**************************************"<<endl;
#endif
#ifdef VIDHYA
cout<<"In cse502.cc fetch"<<endl;
#endif
	sc_addr tempRIP = RIP;

	if (RIP % 16 != 0)
	{
		tempRIP = RIP & ~15;
	}

	for (int port = 0; port < i_ports; ++port)
	{
		if (firstRunFlag)
		{
			i_op[port] = READ;
			i_tagin[port] = tempRIP;
			i_addr[port] = tempRIP;
			prev_tag[port] = RIP;
			firstRunFlag = false;
			last_inst_address = RIP.to_uint64();
			//cout << "base in fetch : "<< RIP<<endl;
		}

		if (i_ready[port])
		{
			for (int p = 0; p < i_ports; ++p)
			{
				if (i_tagout[port] == (prev_tag[p] & ~15))
				{
#ifdef SUDHIR
					cout<<"Fetch from : "<<std::hex<<i_tagout[port].read()<<std::dec <<"\n";
#endif
					//cout<<i_data[port].read()<<endl;
					fetch_decode_shared_buffer = i_data[port].read();
					if (prev_tag[p] != i_tagout[port])
					{ // 1st run misalignment
						//cout<< "Fetch: In 1st run misalignment : \n";
						//cout<<i_data[port].read().range((prev_tag[p] - i_tagout[port])*8,0)<<endl;
						fetch_decode_shared_buffer = i_data[port].read().range((prev_tag[p] - i_tagout[port])*8,0);
						//cout<< "Fetch: 1st run misalignment end\n";
					}
#ifdef SUDHIR
					cout<<fetch_decode_shared_buffer<<endl;
#endif
					prev_tag[p] = 0;
					RIP = tempRIP + 16;
					i_op[port] = READ;
					i_tagin[port] = RIP;
					i_addr[port] = RIP;
					prev_tag[port] = RIP;

					outOfBytes = false;
					return true;
				}
			}
		}
	}
	return false;	
#ifdef VIDHYA
cout<<"In cse502.cc fetch complete"<<endl;
#endif
}

bool CoreCSE502::decode()
{
#ifdef VIDHYA
	cout<<"cse502 Decode start :\n";
	cout<<fetch_decode_shared_buffer.to_string()<<endl;
#endif
	if (first_run_for_decode == true)
	{
        offset = fetch_decode_shared_buffer.length() - 65;
        first_run_for_decode = false;
	}
	else
	{
		offset = fetch_decode_shared_buffer.length() - 1;
	}

    if (jump_flag_for_decoder)
    {
#ifdef SUDHIR
        cout << "before moving offset : "<<offset <<endl;
        cout<<"^^^^^^^^^^^^^^ RiP starting from : " << std::hex << RIP - 16<<" jump address : " << last_inst_address<<std::dec<<endl;
#endif
        moveOffset(last_inst_address - (RIP - 16));
#ifdef SUDHIR
        cout << "after moving "<< last_inst_address - (RIP - 16) << "bytes, offset = "<<offset<<endl;
#endif
        jump_flag_for_decoder = false;
    }

    /*if (state == ResetState)
        state = doResetState();
    if (state == FromCacheState)
        state = doFromCacheState();*/

    //While there's still something to do...
    while (!instDone && !outOfBytes) {
        std::string nextByte = getNextByte();
        switch (state) {
          case PrefixState:
            state = doPrefixState(nextByte);
            break;
          case OpcodeState:
            state = doOpcodeState(nextByte);
            break;
          case ModRMState:
            state = doModRMState(nextByte);
            break;
          case SIBState:
            state = doSIBState(nextByte);
            break;
          case DisplacementState:
            state = doDisplacementState();
            break;
          case ImmediateState:
            state = doImmediateState();
            break;
          case ErrorState:
            printf("Went to the error state in the decoder.\n");
          default:
            printf("Unrecognized state! %d\n", state);
        }
    }
}

void CoreCSE502::work() {
	if (reset) return;
#ifdef VIDHYA
cout<<"In cse502.cc do_work"<<endl;
#endif
//	cout<<"in work\n";
#ifdef PROG2	
    if (RIP > 0x400a9a) 
        return;
#endif

#if PROG1
	if (RIP > 0x4000c5) {
        	return;	
	}
#endif	

#if 1
	bool returned_value = false;

	returned_value = fetch();
	if (returned_value)
		decode();

	if (dcache_read_request) {
		for (int port = 0;port < d_ports; ++port) {
			if (d_ready[port]) {
				sc_addr temp_read = d_data[port].read();
#ifdef SUDHIR                
				cout << "Data read from d_cache : "<<std::hex << temp_read << std::dec<<endl;
#endif                
				dcache_read_request = false;
				cache_read_done = true;
				dcopy.valid = true;
				dcopy.data = temp_read; 
			}
		}

		if ((!dcache_read_request) && (!inst_queue.empty())) {
			process_instruction();
		}
	
	} else if (!inst_queue.empty()) {
		process_instruction();
	}
#endif
#if 0
    if (d_cache_state == Read_State)
    {
        for (int port = 0; port < i_ports; ++port)
            if (!d_ready[port])
                return;
            else
            {
                sc_addr temp_read = d_data[port].read();
                cout << "Data read from d_cache : "<<std::hex << temp_read << std::dec<<endl;
                d_cache_state = Normal_State;

                do 
                {
                    d_op[port] = WRITE;
                    d_tagin[port] = 67107840 -8;
                    d_addr[port] = 67107840-8;
                    cout <<"writing to dcache \n";
                    d_dout[port].write(0x0102030405060708);
                }while(0);
            }
    }


    for (int port = 0; port < i_ports; ++port)
    {
        d_op[port] = READ;
        d_tagin[port] = 67107840;
        d_addr[port] = 67107840;
        d_cache_state = Read_State;
    }
#endif
#ifdef VIDHYA
cout<<"In cse502.cc do_work coplete"<<endl;
#endif
}

void  CoreCSE502::callq_reset() {

	to_add_inst_size = 0;
        current_instruction.rex.w = current_instruction.rex.r = current_instruction.rex.x = current_instruction.rex.b = 0;
        current_instruction.opcode.num = 0;
        current_instruction.opcode.prefixA = current_instruction.opcode.prefixB = current_instruction.opcode.op = "";
        immediateCollected = 0;
        immediateSize = 0;
        displacementSize = 0;
        current_instruction.immediate = 0;
        current_instruction.displacement = 0;
        current_instruction.dispSize = 0;
        current_instruction.opSize = 0;
        current_instruction.addrSize = 0;
        current_instruction.stackSize = 0;
        current_instruction.dispSize = 0;
        current_instruction.modRM.mod = current_instruction.modRM.reg = current_instruction.modRM.rm = "";
        current_instruction.sib.scale = current_instruction.sib.base = current_instruction.sib.index = "";
        current_instruction.immed_flag = false;
        state = PrefixState;

        instDone = false;

}

CoreCSE502::State CoreCSE502::doResetState()
{
    if (instDone == true) 
    {
#ifdef SUDHIR        
        cout << "$$$$ before storing last_inst_address : "<<std::hex<<last_inst_address<<" last_inst_size : " <<std::dec <<last_inst_size<<endl;
#endif        
    	current_instruction.instruction_address = last_inst_address + last_inst_size;
#ifdef SUDHIR
	   cout << "************* END OF INSTRUCTION : " << std::hex<<current_instruction.instruction_address<<std::dec<<endl;
#endif
        last_inst_address = current_instruction.instruction_address;
        last_inst_size = to_add_inst_size + current_instruction.opcode.num + immediateSize + displacementSize;
        current_instruction.next_instruction_address = current_instruction.instruction_address + last_inst_size;
        inst_queue.push(current_instruction);

	    to_add_inst_size = 0;
        current_instruction.rex.w = current_instruction.rex.r = current_instruction.rex.x = current_instruction.rex.b = 0;
        current_instruction.opcode.num = 0;
        current_instruction.opcode.prefixA = current_instruction.opcode.prefixB = current_instruction.opcode.op = "";
        immediateCollected = 0;
        immediateSize = 0;
        displacementSize = 0;
        current_instruction.immediate = 0;
        current_instruction.displacement = 0;
        current_instruction.dispSize = 0;
        current_instruction.opSize = 0;
        current_instruction.addrSize = 0;
        current_instruction.stackSize = 0;
        current_instruction.dispSize = 0;
        current_instruction.modRM.mod = current_instruction.modRM.reg = current_instruction.modRM.rm = "";
        current_instruction.sib.scale = current_instruction.sib.base = current_instruction.sib.index = "";
        current_instruction.immed_flag = false;
        state = PrefixState;

        instDone = false;
    }    
    return state;
}

CoreCSE502::State CoreCSE502::doPrefixState(std::string nextByte)
{

    uint8_t prefix = 0;	
    if (inst_prefix_table.find(nextByte) == inst_prefix_table.end())
    {
        prefix = 0;
    }
    else
    {
        prefix = inst_prefix_table.find(nextByte)->second;
        to_add_inst_size++;
    }

    State nextState = PrefixState;

    if (prefix)
        moveOffset();

    switch(prefix)
    {
        //Operand size override prefixes
      case OperandSizeOverride:
#ifdef SUDHIR
        cout<<"Found operand size override prefix. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.op = true;
        break;
      case AddressSizeOverride:
#ifdef SUDHIR 
	cout<<"Found address size override prefix. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.addr = true;
        break;
        //Segment override prefixes
      case CSOverride:
      case DSOverride:
      case ESOverride:
      case FSOverride:
      case GSOverride:
      case SSOverride:
#ifdef SUDHIR
        cout<<"Found segment override. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.seg = true;
        break;
      case Lock:
#ifdef SUDHIR
        cout<<"Found lock prefix. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.lock = true;
        break;
      case Rep:
#ifdef SUDHIR
        cout<<"Found rep prefix. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.rep = true;
        break;
      case Repne:
#ifdef SUDHIR
        cout<<"Found repne prefix. : "<<nextByte <<"\n";
#endif
        current_instruction.legacy.repne = true;
        break;
      case RexPrefix:
#ifdef SUDHIR
        cout<<"Found Rex prefix. : "<<nextByte <<"\n";
#endif
    	current_instruction.rex.w = nextByte.c_str()[4];
		current_instruction.rex.r = nextByte.c_str()[5];
		current_instruction.rex.x = nextByte.c_str()[6];
		current_instruction.rex.b = nextByte.c_str()[7];        
        break;
      case 0:
      	//cout<<"Setting next state as OpcodeState\n";
        nextState = OpcodeState;
        break;
      default:
        cout<<"Unrecognized prefix : " << nextByte << endl;
    }
    return nextState;
}

CoreCSE502::State CoreCSE502::doOpcodeState(std::string nextByte)
{
    State nextState = ErrorState;
    current_instruction.opcode.num++;

    moveOffset();

    if(current_instruction.opcode.num == 1 && nextByte == "00001111")
    {
        nextState = OpcodeState;
#ifdef SUDHIR
        cout<<"Found two byte opcode. : "<<nextByte <<"\n";
#endif
        current_instruction.opcode.prefixA = nextByte;
    }
    else if(current_instruction.opcode.num == 2 && (nextByte == "00111000" || nextByte == "00111010"))
    {
        nextState = OpcodeState;
#ifdef SUDHIR
        cout<<"Found three byte opcode. : "<<nextByte <<"\n";
#endif
        current_instruction.opcode.prefixB = nextByte;
    }
    else
    {
#ifdef SUDHIR
        printf("Opcode byte : %s\n", nextByte.c_str());
#endif
        //cout<<"Found opcode : "<< nextByte <<endl;
        current_instruction.opcode.op = nextByte;

        uint64_t logOpSize, logAddrSize;
	string lookup_string = "";

	if (current_instruction.rex.w == '1')
	{
		lookup_string = "1";
	}
	else
	{
		lookup_string = "0";
	}
	if (nextByte.c_str()[6] == '1')
	{
		lookup_string += "1";
	}
	else
	{
		lookup_string += "0";
	}
	if (nextByte.c_str()[7] == '1')
	{
		lookup_string += "1";
	}
	else
	{
		lookup_string += "0";
	}
	
	logOpSize = opsize_table.find(lookup_string)->second;
	logAddrSize = addrsize_table.find(lookup_string)->second;
#ifdef SUDHIR 
	cout <<"lookup_string : "<< lookup_string<< "logOpSize : "<< logOpSize << "logAddrSize : " << logAddrSize << endl;
#endif 

        current_instruction.opSize = 1 << logOpSize;
        current_instruction.addrSize = 1 << logAddrSize;
        
        current_instruction.stackSize = 0;

        int immType;

        //cout << "Opcode num : "<<  current_instruction.opcode.num<<endl;
        
        if ((current_instruction.opcode.num - 1) == 0)
        {
                if (one_byte_immediate_type_table.find(nextByte) == one_byte_immediate_type_table.end())
                        immType = 0;
                else
                        immType = one_byte_immediate_type_table.find(nextByte)->second;
        }
        else if ((current_instruction.opcode.num - 1) == 1)
        {
                if (two_byte_immediate_type_table.find(nextByte) == two_byte_immediate_type_table.end())
                        immType = 0;
                else
                        immType = two_byte_immediate_type_table.find(nextByte)->second;
        }
#ifdef SUDHIR
        printf("Immtype, addrsize, opsize : %d\t%d\t%d\n", immType, current_instruction.addrSize, current_instruction.opSize);
#endif                
        if (current_instruction.opcode.num == 1 && bin2dec(nextByte) >= 0xA0 && bin2dec(nextByte) <= 0xA3)
	{
#ifdef SUDHIR        
	    //printf("here size_table[%d][%d] \n", logAddrSize - 1, immType);
#endif
            immediateSize = SizeTypeToSize[logAddrSize - 1][immType];
        }
	else
        {
#ifdef SUDHIR            
	    //printf("there size_table[%d][%d] \n", logOpSize - 1, immType);
#endif
	    immediateSize = SizeTypeToSize[logOpSize - 1][immType];
       	}
#ifdef SUDHIR
	printf("$$$$$$$$$$$$$$$$ Immediate size : %d\n", immediateSize);
#endif
        int uses_mod_rm;     
        if ((current_instruction.opcode.num - 1) == 0)
        {
#ifdef SUDHIR            
                //cout <<"coming here \n";
#endif            
                uses_mod_rm = one_byte_uses_modrm.find(nextByte)->second;
        }
        else
        {
#ifdef SUDHIR            
                //cout <<"ass here\n";
#endif            
                uses_mod_rm = two_byte_uses_modrm.find(nextByte)->second;
        }

        //Determine what to expect next
        if (uses_mod_rm == Yes) 
        {
            nextState = ModRMState;
            to_add_inst_size++;
        } 
        else 
        {
#ifdef SUDHIR            
            //cout << "Yay! no modrm immediateSize : "<<immediateSize<<"\n";
#endif            
            if(immediateSize) 
            {
                nextState = ImmediateState;
            } 
            else 
            {
                instDone = true; //############### current instruction done, memset it and move on in life
                nextState = doResetState();
            }
        }

    }
    return nextState;
}

CoreCSE502::State CoreCSE502::doModRMState(std::string nextByte)
{
    State nextState = ErrorState;

    current_instruction.modRM.mod = nextByte.substr(0,2);
    current_instruction.modRM.reg = nextByte.substr(2,3);
    current_instruction.modRM.rm = nextByte.substr(5,3);
#ifdef SUDHIR
    cout<<"ModRM Byte: "<<nextByte<<endl;
#endif
    // To calculate Displacement size to understand the concept later
    if ((current_instruction.modRM.mod == "00" && current_instruction.modRM.rm == "101") || current_instruction.modRM.mod == "10")
        displacementSize = 4;
    else if (current_instruction.modRM.mod == "01")
        displacementSize = 1;
    else
        displacementSize = 0;

    std::bitset<3> a (string (current_instruction.modRM.reg)); 
    std::bitset<3> b(string("110"));
    std::bitset<3> c = a | b;
    std::bitset<3> comp_to (string("000"));

    // The "test" instruction in group 3 needs an immediate, even though
    // the other instructions with the same actual opcode don't.
    if (current_instruction.opcode.num == 1 && (c == comp_to)) 
    {
       if (current_instruction.opcode.op == "11110110")
           immediateSize = 1;
       else if (current_instruction.opcode.op == "11110111")
           immediateSize = (current_instruction.opSize == 8) ? 4 : current_instruction.opSize;
    }

    //If there's an SIB, get that next.
    //There is no SIB in 16 bit mode.
    if (current_instruction.modRM.rm == "100" && current_instruction.modRM.mod != "11") {
            // && in 32/64 bit mode)
        nextState = SIBState;
    } else if(displacementSize) {
        nextState = DisplacementState;
    } else if(immediateSize) {
        nextState = ImmediateState;
    } else {
        instDone = true;
        nextState = doResetState();
    }

    moveOffset();
    return nextState;
}

CoreCSE502::State CoreCSE502::doSIBState(std::string nextByte)
{
    State nextState = ErrorState;

    current_instruction.sib.scale = nextByte.substr(0,2);
    current_instruction.sib.index = nextByte.substr(2,3);
    current_instruction.sib.base = nextByte.substr(5,3);
#ifdef SUDHIR    
    cout<<"SIB byte : "<<nextByte<<endl;
#endif
    moveOffset();
    
    if (current_instruction.modRM.mod == "00" && current_instruction.sib.base == "101")
        displacementSize = 4;
    if (displacementSize) {
        nextState = DisplacementState;
    } else if(immediateSize) {
        nextState = ImmediateState;
    } else {
        instDone = true;
        nextState = doResetState();
    }
    
    return nextState;
}

CoreCSE502::State CoreCSE502::doDisplacementState()
{
    State nextState = ErrorState;

    getImmediate(immediateCollected,
            current_instruction.displacement,
            displacementSize);
#ifdef SUDHIR
    //cout<<"Collecting " << displacementSize <<" byte displacement, got "<<immediateCollected <<" bytes.\n";
#endif
    if(displacementSize == immediateCollected) 
    {
        //Reset this for other immediates.
        immediateCollected = 0;
        //Sign extend the displacement
#ifdef SUDHIR        
        cout << "Displacement Byte (before sign extension) : "<< current_instruction.displacement<<endl;
#endif        
        switch(displacementSize)
        {
          case 1:
            current_instruction.displacement = sext<8>(current_instruction.displacement);
            break;
          case 2:
            current_instruction.displacement = sext<16>(current_instruction.displacement);
            break;
          case 4:
            current_instruction.displacement = sext<32>(current_instruction.displacement);
            break;
          default:
            cout<<"Undefined displacement size!\n";
        }
#ifdef SUDHIR        
        //cout<<"Collected displacement " <<  current_instruction.displacement << endl;
#endif        
        if(immediateSize) {
            nextState = ImmediateState;
        } else {
            instDone = true;
            nextState = doResetState();
        }

        current_instruction.dispSize = displacementSize;
    }
    else
        nextState = DisplacementState;
    return nextState;
}

CoreCSE502::State CoreCSE502::doImmediateState()
{
    State nextState = ErrorState;

    current_instruction.immed_flag = true;
    getImmediate(immediateCollected,
            current_instruction.immediate,
            immediateSize);
    
#ifdef SUDHIR
    cout<<"Collecting "<< immediateSize <<" byte immediate, got "<<immediateCollected<<" bytes.\n";
#endif

    if(immediateSize == immediateCollected)
    {
        //Reset this for other immediates.
        immediateCollected = 0;

        //XXX Warning! The following is an observed pattern and might
        //not always be true!

        //Instructions which use 64 bit operands but 32 bit immediates
        //need to have the immediate sign extended to 64 bits.
        //Instructions which use true 64 bit immediates won't be
        //affected, and instructions that use true 32 bit immediates
        //won't notice.
#ifdef SUDHIR
        cout << " Immediate Byte (before sign extension): " << current_instruction.immediate<<endl;
#endif        
        switch(immediateSize)
        {
          case 4:
            current_instruction.immediate = sext<32>(current_instruction.immediate);
            break;
          case 1:
            current_instruction.immediate = sext<8>(current_instruction.immediate);
        }
#ifdef SUDHIR
        cout <<"Collected immediate " << current_instruction.immediate<<"\n";
#endif        
        instDone = true;
        nextState = doResetState();
    }
    else
        nextState = ImmediateState;
    return nextState;
}

uint8_t get_rex(Instruction instr) {
	uint8_t ret_val = 0;
	if ((instr.rex.w == 0)&&(instr.rex.r == 0)&&(instr.rex.x == 0)&&(instr.rex.b == 0)) {
#ifdef SUDHIR        
		cout<<"No rex byte\n";
#endif        
		return ret_val;
	}
	ret_val = (((instr.rex.w-'0')<<3)|((instr.rex.r-'0')<<2)|((instr.rex.x-'0')<<1)|(instr.rex.b-'0'));
	ret_val |= 0x40;
	return ret_val;
}

void CoreCSE502::process_instr(struct Instruction instr) {

	struct execution_struct exec;
	uint64_t src_operand, dst_operand, result;
	uint8_t op_byte;
	std::string src_register, dst_register, op_code;
	op_code = instr.opcode.op;
	op_byte = bin2dec(op_code);
	init_exec_struct(exec);
	exec.opcode = op_byte;
	exec.immediate = instr.immediate;
	exec.curr_instr_address = instr.instruction_address;
    exec.rip = instr.next_instruction_address;
	exec.rex_byte = get_rex(instr);
	if (instr.modRM.mod == "11") {
		exec.r2rmode = true;
	}

	if (instr.modRM.mod == "") {
		execute(exec);
		dump_registers();
		return;

	}

	if (false) {
		std::string extended_type = instr.modRM.reg;
		dst_register = instr.modRM.rm;
		
		
		



	} else {
	uint8_t mod_rm = bin2dec(instr.modRM.mod + instr.modRM.reg + instr.modRM.rm);
	exec.sub_op = (mod_rm>>3)&0x07;
	// mod bit is 11, register to register operation
	if (instr.modRM.mod == "11") {
		if (instr.rex.b == '1') {
			exec.rm_register = "1" + instr.modRM.rm;
		} else {
			exec.rm_register = "0" + instr.modRM.rm;
		}
		if (instr.rex.r == '1') {
			exec.reg_register = "1" + instr.modRM.reg;
		} else {
			exec.reg_register = "0" + instr.modRM.reg;
		}
		exec.direction = (op_byte>>1)&0x01;
//		rm_register = instr.rex.b + instr.modRM.rm;
//		printf("Value of rex.b is %d\n", instr.rex.b);
//		cout<<"Value of rex.b is "<<instr.rex.b;
#ifdef SUDHIR
		cout<<"In register to register mode\n";
#endif
/*		if (op_code[6] == '0') {  //src --> reg dest ---> rm
			src_register = instr.modRM.reg;
			dst_register = instr.modRM.rm;
			if (instr.rex.r == '1') {
				src_register = "1" + src_register;
			} else {
				src_register = "0" + src_register;
			}

			if (instr.rex.b == '1') {
				dst_register = "1" + dst_register;
			} else {
				dst_register = "0" + dst_register;
			}
		} else {
			src_register = instr.modRM.rm;
			dst_register = instr.modRM.reg;
			if (instr.rex.r == '1') {
				dst_register = "1" + dst_register;
			} else {
				dst_register = "0" + dst_register;
			}
			
			if (instr.rex.b == '1') {
				src_register = "1" + src_register;
			} else {
				src_register = "0" + src_register;
			}
		}

		exec.dst_register = dst_register;
		if (instr.immed_flag) {
			exec.immediate = instr.immediate;
		} else {
			exec.src_operand = register_map_file.find(src_register)->second;
		}
		exec.dst_operand = register_map_file.find(dst_register)->second;
		// here goes my hack
		if (op_byte == 0xc7) {
			exec.dst_register = src_register;
		}
*/		execute(exec);
//		cout<<"Source Register: "<<src_register<<" destination register: "<<exec.dst_register<<endl;
		dump_registers();
//		cout<<"Souce operand: "<<src_operand<<"  destination operand:  "<<dst_operand<<"  and the result is "<<result<<endl;

	} else { 
		if (false) {
/*
			exec.src_operand = instr.immediate;
			exec.immediate = instr.immediate;
			if (op_code[6] == '0') { // destination encoded in rm
				uint64_t rm_address = calculate_memory_address(instr);
				exec.address = rm_address;
				exec.dst_operand = read_memory(rm_address);
				execute(exec);
				dump_registers();
			} else {  //destination encoded in reg
				dst_register = instr.modRM.reg;
				if (instr.rex.r == '1') {
					dst_register = "1" + dst_register;
				} else {
					dst_register = "0" + dst_register;
				}
				exec.dst_operand = register_map_file.find(dst_register)->second;
				exec.dst_register = dst_register;
				execute(exec);
				dump_registers();
			} */
			// read destination operand, check 1st if it is in memory or register	
		} else {  //one operand is in memory and other is in register
			exec.direction = (op_byte>>1)&0x01;
			if (get_rex(instr) == 0) {
				exec.reg_register = '0' + instr.modRM.reg;
				exec.rm_register = '0' + instr.modRM.rm;
			} else {
				if (instr.rex.r == '1') {
					exec.reg_register = '1' + instr.modRM.reg;
				} else {
					exec.reg_register = '0' + instr.modRM.reg;
				}

				if (instr.rex.b == '1') {
					exec.rm_register = '1' + instr.modRM.rm;
				} else {
					exec.rm_register = '0' + instr.modRM.rm;
				}
			}

			uint64_t address = calculate_memory_address(instr);
			//cout<<"The address calculated is "<<std::hex<<address<<std::dec<<endl;
			exec.address = address;
			execute(exec);
			dump_registers();

/*			if (op_code[6] == '0') { // registrer to memory
				src_register = instr.modRM.reg;
				if (instr.rex.r == '1') {
					src_register = "1" + src_register;
				} else {
					src_register = "0" + src_register;
				}
				exec.src_operand = register_map_file.find(src_register)->second;
				uint64_t address = calculate_memory_address(instr);
				exec.address = address;
				exec.dst_operand = read_memory(address);
				execute(exec);
				dump_registers();
				// read the dst operand from this address and write back the data to the address
			//	get the destination operand from memory location
			} else {  // destination is a register
				dst_register = instr.modRM.reg;
				if (instr.rex.r == '1') {
					dst_register = "1" + dst_register;
				} else {
					dst_register = "0" + dst_register;
				}
				exec.dst_operand = register_map_file.find(dst_register)->second;
				uint64_t address = calculate_memory_address(instr);
				exec.src_operand = read_memory(address);
				exec.dst_register = dst_register;
				execute(exec);
				dump_registers();
				// read the src operand from this address
			} */

		}

		}

}
}

uint64_t CoreCSE502::calculate_memory_address(Instruction instr) {
	std::string rm_register = instr.modRM.rm;
	uint64_t displacement = instr.displacement;
	uint64_t address, sib_address;

	if (instr.modRM.rm == "100") {
		if ((instr.modRM.mod == "01") || (instr.modRM.mod == "10")) {
			sib_address = process_sib(instr);
			address = displacement + sib_address;
		} else if (instr.modRM.mod == "00") {
			sib_address = special_process_sib(instr); // need to be added code here
			address = displacement + sib_address;
		}
	} else if ((instr.modRM.rm == "101") && (instr.modRM.mod == "00")){
		address = instr.next_instruction_address + displacement; // need to figure out a way to get RIP
	} else {
		if (instr.rex.b == '1') {
			rm_register = "1" + rm_register;
		} else {
			rm_register = "0" + rm_register;
		}
		address = displacement + register_map_file.find(rm_register)->second;
	}

	return address;
}

uint64_t CoreCSE502::special_process_sib(Instruction instr) {

	Sib sib = instr.sib;
	std::string sib_string, index_register, base_register;
	sib_string = sib.scale + sib.index + sib.base;
	index_register = sib.index;
	base_register = sib.base;
	uint8_t sib_byte = bin2dec(sib_string);
	uint8_t scale = sib_byte>>6;
	uint64_t index, base, address;

	if (instr.rex.x == '1') {
		index_register = "1" + index_register;
	} else {
		index_register = "0" + index_register;
	}

	if (index_register == "0100") {
		index = 0;
	} else {	
		index = register_map_file.find(index_register)->second;
	}	
	
 	if (instr.rex.b == '1') {
		base_register = "1" + base_register;
	} else {
		base_register = "0" + base_register;
	}

	if ((base_register == "0101") || (base_register == "1101")) {
		base = 0;
	} else {
		base = register_map_file.find(base_register)->second;
	}

	address = base + index*(1<<scale);
	
	return address;
}

uint64_t CoreCSE502::process_sib(Instruction instr) {

	Sib sib = instr.sib;
	std::string sib_string, index_register, base_register;
	sib_string = sib.scale + sib.index + sib.base;
	index_register = sib.index;
	base_register = sib.base;
	uint8_t sib_byte = bin2dec(sib_string);
	uint8_t scale = sib_byte>>6;
	uint64_t index, base, address;

	if (instr.rex.x == '1') {
		index_register = "1" + index_register;
	} else {
		index_register = "0" + index_register;
	}

	if (index_register == "0100") {
		index = 0;
	} else {
		index = register_map_file.find(index_register)->second;
	}

	if (instr.rex.b == '1') {
		base_register = "1" + base_register;
	} else {
		base_register = "0" + base_register;
	}
	base = register_map_file.find(base_register)->second;

	address = base + index*(1<<scale);

	return address;
}

void CoreCSE502::print_instr_content(Instruction instr) {
#ifdef SUDHIR
    cout<<"**********************************************\n";
    cout<<"Immediate\tDisplacement\tOpcode\tMod\tReg\trm"<<endl;
    cout<<instr.immediate<<"\t\t"<<instr.displacement<<"\t\t"<<instr.opcode.op<<"\t\t"<<instr.modRM.mod<<"\t\t"<<instr.modRM.reg<<"\t\t"<<instr.modRM.rm<<endl;
    cout<<"**********************************************\n";
#endif
}

void CoreCSE502::process_instruction() {
	if (inst_queue.empty()) {
#ifdef SUDHIR
		cout<<"The queue is empty\n";
#endif
	} else {
		Instruction instr = inst_queue.front();
#ifdef VINAY
		cout<<"Current instruction address "<<std::hex<<instr.instruction_address<<std::dec<<endl;
        cout<<"Next instruction address "<<std::hex<<instr.next_instruction_address<<std::dec<<endl;
#endif
		print_instr_content(instr);
		process_instr(instr);
		if (!dcache_read_request) {
			if (!inst_queue.empty()) {
#ifdef SUDHIR
				cout<<"Size of queue"<<inst_queue.size()<<endl;
#endif
				inst_queue.pop();
			}
		}
//		inst_queue.pop();
	}
}

void CoreCSE502::execute(struct execution_struct &exec) {

	switch(exec.opcode) {
        case 0x01:
            handle_add(exec);
            break;
		case 0x50:
		case 0x51:
		case 0x52:
		case 0x53:
		case 0x54:
		case 0x55:
		case 0x56:
		case 0x57:
			handle_push(exec);
			break;
		case 0x58:
		case 0x59:
		case 0x5a:
		case 0x5b:
		case 0x5c:
		case 0x5d:
		case 0x5e:
		case 0x5f:
			handle_pop(exec);
			break;
		case 0x31:
			handle_xor(exec);
			break;
		case 0x89:
		case 0x8b:
			handle_move(exec);
			break;
		case 0xc7:
		case 0xb8:
		case 0xb9:
		case 0xba:
		case 0xbb:
		case 0xbc:
		case 0xbd:
		case 0xbe:
		case 0xbf:
			handle_immed_move(exec);
			break;
		case 0x8c:
			handle_jl(exec);
			break;
		case 0x7d:
			handle_jge(exec);
			break;
		case 0x8e:
			handle_jle(exec);
			break;
		case 0x8f:
			handle_jg(exec);
			break;
		case 0x81:
			switch(exec.sub_op) {
				case 0x04:
					handle_and(exec);
					break;
				default:
					cout<<"Invald sub op\n";
					exit(1);
			}
			break;
		case 0x83:
			switch(exec.sub_op) {
				case 0x0:
					handle_add(exec);
					break;
				case 0x04:
					handle_and(exec);
					break;
				case 0x07:
					handle_compare(exec);
					break; 
				default:
					cout<<"Invalid sub op\n";
			exit(0);
			}
			break;
		case 0x25:
			handle_and(exec);
			break;
		case 0x05:
			handle_syscall(exec);
			break;
		case 0xc3:
			handle_retq();
			break;
		case 0xc1:
			switch(exec.sub_op) {
				case 0x05:
					handle_shr(exec); //shr
					break;
				case 0x07:
					handle_sar(exec); //sar
					break;
			}
			break;
		case 0xff:
			switch(exec.sub_op) {
				case 0x00:
					handle_increment(exec);
					break;
				case 0x01:
					handle_decrement(exec);
					break;
				case 0x02:
				case 0x03:
					handle_call_abs(exec);
					break;
				case 0x04:
				case 0x05:
					handle_jump_absolute(exec);
					break;
				default:
					cout<<"Invalid sub opcode\n";
			exit(0);
			}
			break;
		case 0xe8:
			handle_call_relative(exec);
			break;
		case 0x77:
			handle_ja(exec);
			break;
		case 0x74:
			handle_je(exec);
			break;
		case 0x85:
            handle_test(exec);
            break;
		case 0x75:
			handle_jne(exec);
			break;
		case 0x72:
			handle_jb(exec);
			break;
		case 0xeb:
		case 0xe9:
			handle_jump(exec);
			break;
		case 0x8d:
			handle_lea(exec);
			break;
		case 0x1f:
			handle_nopl(exec);
			break;
		case 0x45:
			handle_cmovne(exec);
			break;
		case 0x49:
			handle_cmovns(exec);
			break;
		case 0x4f:
			handle_cmovg(exec);
			break;			
		case 0xd1:
			switch(exec.sub_op) {
				case 0x04:
					handle_shl(exec);
					break;
				default:
					cout<<"Invalid sub op code\n";
			exit(0);
			}
			break;
        case 0xF7:
            switch(exec.sub_op) {
                case 0x0:
                case 0x1:
                    handle_test(exec);
                    break;
                case 0x3:
                    handle_neg(exec);
                    break;
                case 0x5:
                    handle_imul(exec);
                    break;
                case 0x7:
                    handle_idiv(exec);
                    break;
                default:
                    cout<<"Invalid sub opcode\n";                
			exit(0);
            }
            break;
		case 0x29:
		case 0x2b:
			handle_sub(exec);
			break;
		case 0x3b:
		case 0x39:
			handle_compare(exec);
			break;
        case 0x6b:
        case 0xaf:
            handle_imul(exec);
            break;
		default:
			cout<<"Invalid opcode\n";
			exit(0);

	}
}

void CoreCSE502::handle_imul(struct execution_struct &exec)
{

}

void CoreCSE502::handle_idiv(struct execution_struct &exec) {
    if (exec.opcode == 0xf7) {
        uint64_t dividend = (register_map_file["0010"] & 0xffffffff00000000) | (register_map_file["0000"] & 0x00000000ffffffff);
        uint64_t divider = register_map_file[exec.rm_register];

        if (divider == 0)
        {
            cout << "Divide by 0 error\n";
            exit(1);
        }

        uint64_t temp = dividend/divider;

        /*if ((temp > 0x7FFFFFFFH) || (temp < 0x80000000H)) {
            cout << "Divide error\n";
            exit(1);
            
        } else {*/

            register_map_file["0000"] = dividend % divider; // rax quotient
            register_map_file["0010"] = dividend / divider; // rdx remainder
        //}
    }
}

void CoreCSE502::handle_add(struct execution_struct &exec) {

    long long result;

    if (exec.opcode == 0x83) {
        register_map_file[exec.rm_register] = result = register_map_file[exec.rm_register] + exec.immediate;
    } else if (exec.opcode == 0x01) {
        register_map_file[exec.rm_register] = result = register_map_file[exec.rm_register] + register_map_file[exec.reg_register];
    }

    if (result == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    if ((result & (1ULL << 63)) == (1ULL<<63)) {
        flag.sign_flag = true;
    } else {
        flag.sign_flag = false;
    }

    if (result > 0xffffffffffffffff) {
        flag.carry_flag = true;
        flag.overflow_flag = true;
    } else if ((result < 0xffffffffffffffff) && (result > 0)) {
        flag.carry_flag = false;
        flag.overflow_flag = false;
    }

    
}

void CoreCSE502::handle_compare(struct execution_struct &exec) {

    uint64_t operand_1, operand_2;

    if (exec.opcode ==  0x83) {
        operand_1 = register_map_file[exec.rm_register];
        operand_2 = exec.immediate;

    } else if (exec.opcode == 0x3b) {
        operand_1 = register_map_file[exec.reg_register];;
        operand_2 = register_map_file[exec.rm_register];
    } else if (exec.opcode == 0x39){
        operand_1 = register_map_file[exec.rm_register];
        operand_2 = register_map_file[exec.reg_register];;
    }
    long long difference = operand_1 - operand_2;

    //cout << "CMP : difference :" << difference << " : " << (int64_t)operand_1 - (int64_t)operand_2<<endl;
    if (difference == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    if (difference & (1ULL<<63) == (1ULL<<63)) {
        flag.sign_flag = true;
    } else {
        flag.sign_flag = false;
    }

    if (difference < 0) {
        flag.carry_flag = true;
    } else {
        flag.carry_flag = false;
    }

}

void CoreCSE502::handle_sub(struct execution_struct &exec) {
    if (exec.opcode == 0x29) {
        uint64_t operand_1 = register_map_file[exec.rm_register];
        uint64_t operand_2 = register_map_file[exec.reg_register];

        long long difference = operand_1 - operand_2;

        if (difference == 0) {
            flag.zero_flag = true;
        } else {
            flag.zero_flag = false;
        }

        if (difference & (1ULL<<63) == (1ULL<<63)) {
            flag.sign_flag = true;
        } else {
            flag.sign_flag = false;
        }
#if 1
        // if both are unsigned values and operand 1 is less than operand 2
        if (difference < 0) {
            flag.carry_flag = true;
        } else {
            flag.carry_flag = false;
        }
#endif
        register_map_file[exec.rm_register] = difference; // 128 bit result stored in 64 bit ???????? Correct ??

    } else if (exec.opcode == 0x2b) {
        if (cache_read_done) {
                if (dcopy.valid) {
                        uint64_t operand_1 = register_map_file[exec.reg_register];
                        uint64_t operand_2 = dcopy.data;

                        long long difference = operand_1 - operand_2;

                        if (difference == 0) {
                            flag.zero_flag = true;
                        } else {
                            flag.zero_flag = false;
                        }

                        if (difference & (1ULL<<63) == (1ULL<<63)) {
                            flag.sign_flag = true;
                        } else {
                            flag.sign_flag = false;
                        }
#if 1
                        // if both are unsigned values and operand 1 is less than operand 2
                        if (difference < 0) {
                            flag.carry_flag = true;
                        } else {
                            flag.carry_flag = false;
                        }
#endif
                        register_map_file[exec.reg_register] = difference; // 128 bit result stored in 64 bit ???????? Correct ??
                } else {
                        cout<<"This should not happen"<<endl;
                }
                dcache_read_request = false;
		        cache_read_done = false;
        } else {
                read_memory(exec.address);
        }        
    }
}


void CoreCSE502::handle_neg(struct execution_struct &exec) {
    if (register_map_file[exec.rm_register]  == 0) {
        flag.carry_flag = false;
    } else {
        flag.carry_flag = true;
    }

    uint64_t data = register_map_file[exec.rm_register];
    if ((data & (1ULL<<63)) == (1ULL<<63)) {
        flag.overflow_flag = true; ///////////////////////////////// is this needed ??
    } else { 
        register_map_file[exec.rm_register] = -register_map_file[exec.rm_register];
        flag.overflow_flag = false;
    }

    if (register_map_file[exec.rm_register] == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    if (register_map_file[exec.rm_register] & (1ULL << 63) == (1ULL<<63)) {
        flag.sign_flag = true;
    } else {
        flag.sign_flag = false;
    }
}

// No flags effected
void CoreCSE502::handle_lea(struct execution_struct &exec) {
	uint64_t address = exec.address;
	register_map_file[exec.reg_register] = address; 
}

// No flags effected
void CoreCSE502::handle_nopl(struct execution_struct &exec) {
	cout<<"Do nothing\n";
}

// No flags effected
void CoreCSE502::handle_cmovg(struct execution_struct &exec) {
	if ((!flag.zero_flag) && (flag.sign_flag == flag.overflow_flag)) {
		register_map_file[exec.reg_register] = register_map_file[exec.rm_register];
	}
}

// No flags effected
void CoreCSE502::handle_cmovne(struct execution_struct &exec) {
	if (!flag.zero_flag) {
		register_map_file[exec.reg_register] = register_map_file[exec.rm_register];
	}
}

// No flags effected
void CoreCSE502::handle_cmovns(struct execution_struct &exec) {
	if (!flag.sign_flag) {
		register_map_file[exec.reg_register] = register_map_file[exec.rm_register];
	}
}

// No flags effected
void CoreCSE502::handle_push(struct execution_struct &exec) {
	uint8_t register_num = exec.opcode - 0x50;
	if (exec.rex_byte != 0) {
		register_num += 8;
	}
	uint64_t data = register_map_file[register_string[register_num]];
	register_map_file["0100"] -= 8;
	debug_stack.push(data);
	write_back(register_map_file["0100"], data);
}

// No flags effected
void CoreCSE502::handle_pop(struct execution_struct &exec) {
	if (cache_read_done) {
		debug_stack_print();
		if (debug_stack.empty()) {
			;//cout<<"Debug stack empty\n";
		} else {
			debug_stack.pop();
		}
		uint8_t register_num = exec.opcode - 0x58;
		if (exec.rex_byte != 0) {
			register_num += 8;
		}
		register_map_file["0100"] += 8;
		if (dcopy.valid) {
			register_map_file[register_string[register_num]] = dcopy.data;
		} else {
			cout<<"This is weired, this should not happen\n";
		}
		dcache_read_request = false;
		cache_read_done = false;
	} else {
		read_memory(register_map_file["0100"]);
	}
}

// No flags effected
void CoreCSE502::handle_jump(struct execution_struct &exec) {
    uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
    modifyRIP(jump_address);
}

void CoreCSE502::handle_jump_absolute(struct execution_struct &exec) {
	
	if (cache_read_done) {
		uint64_t jump_address;
		debug_stack_print();
		if (debug_stack.empty()) {
			;//cout<<"Debug stack empty\n";
		} else {
			debug_stack.pop();
		}
		if (dcopy.valid) {
			jump_address = dcopy.data;
		} else {
			cout<<"This is weired, this should not happen\n";
		}
		dcache_read_request = false;
		cache_read_done = false;
		modifyRIP(jump_address);	
	} else {
		read_memory(exec.immediate);
	}
}

void CoreCSE502::handle_xor(struct execution_struct &exec) {
	uint64_t result = register_map_file[exec.rm_register]^register_map_file[exec.reg_register];
	if (exec.direction == 1) {
		register_map_file[exec.reg_register] = result;
	} else {
		register_map_file[exec.rm_register] = result;
	}

	flag.overflow_flag = flag.carry_flag = false;
	if (result == 0) {
		flag.zero_flag = true;
	} else {
		flag.zero_flag = false;
	}

	if ((result & (1ULL<<63)) == (1ULL<<63)) {
		flag.sign_flag = true;
	} else {
		flag.sign_flag = false;
	}
}

void CoreCSE502::handle_and(struct execution_struct &exec) {
	uint64_t result;
	if((exec.opcode == 0x83) || (exec.opcode == 0x81)) {
		result = exec.immediate & register_map_file[exec.rm_register];
		
		register_map_file[exec.rm_register] = result;
	} else {
		result = exec.immediate & register_map_file["0000"];
		register_map_file["0000"] = result;
	}


	flag.overflow_flag = flag.carry_flag = false;
	if (result == 0) {
		flag.zero_flag = true;
	} else {
		flag.zero_flag = false;
	}

	if ((result & (1ULL<<63)) == (1ULL<<63)) {
		flag.sign_flag = true;
	} else {
		flag.sign_flag = false;
	}
}

void CoreCSE502::handle_sar(struct execution_struct &exec) {
	uint64_t operand = register_map_file[exec.rm_register];
	uint8_t carry_flag_bit = (operand >> (exec.immediate-1)) & 0x01;
	if (carry_flag_bit == 1) {
		flag.carry_flag = true;
	} else {
		flag.carry_flag = false;
	}
	
	uint64_t temp_val = operand >> exec.immediate;
	if (((operand >> 63) & 0x01) == 1) {
		temp_val |= ~mask(64 - exec.immediate);
	}
		
	register_map_file[exec.rm_register] = temp_val;
}

void CoreCSE502::handle_shl(struct execution_struct &exec) {
	uint64_t operand = register_map_file[exec.rm_register];
	uint8_t carry_flag_bit = (operand >> 63) & 0x01;
	if (carry_flag_bit == 1) {
		flag.carry_flag = true;
	} else {
		flag.carry_flag = false;
	}

	operand = operand << 1;
	register_map_file[exec.rm_register] = operand;
}

void CoreCSE502::handle_shiftr_one(struct execution_struct &exec) {
	uint64_t operand = register_map_file[exec.rm_register];
	uint8_t carry_flag_bit = operand & 0x01;
	if (carry_flag_bit == 1) {
		flag.carry_flag = true;
	} else {
		flag.carry_flag = false;
	}
	uint64_t temp_val = operand >> 1;
	if (((operand >> 63) & 0x01) == 1) {
		temp_val |= ~mask(63);
	}

	register_map_file[exec.rm_register] = temp_val;
}

void CoreCSE502::handle_shr(struct execution_struct &exec) {
	uint64_t operand = register_map_file[exec.rm_register];
	uint8_t carry_flag_bit = (operand >> (exec.immediate-1)) & 0x01;
	if (carry_flag_bit == 1) {
		flag.carry_flag = true;
	} else {
		flag.carry_flag = false;
	}
	register_map_file[exec.rm_register] = register_map_file[exec.rm_register] >> exec.immediate;
}

void CoreCSE502::handle_increment(struct execution_struct &exec) {

    long long result;

	result = register_map_file[exec.rm_register] = register_map_file[exec.rm_register] + 1;

    if (result == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    if (result < 0) {
        flag.sign_flag = true;
    } else {
        flag.sign_flag = false;
    }

    if (result > 0xffffffffffffffff) {
        flag.overflow_flag = true;
    } else if (result < 0xffffffffffffffff && result > 0) {
        flag.overflow_flag = false;
    }
}

void CoreCSE502::handle_decrement(struct execution_struct &exec) {
    long long result;
	result = register_map_file[exec.rm_register] = register_map_file[exec.rm_register] - 1;

    if (result == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    if (result < 0) {
        flag.sign_flag = true;
    } else {
        flag.sign_flag = false;
    }

    if (result > 0xffffffffffffffff) { 
        flag.overflow_flag = true;
    } else if (result > 0 && result < 0xffffffffffffffff) {
        flag.overflow_flag = false;
    }
}

void CoreCSE502::handle_move(struct execution_struct &exec) {

	if (exec.r2rmode) {
#ifdef SUDHIR        
		cout<<"Register to Register move\n";
#endif        
		if (exec.opcode == 0x89) {
			register_map_file[exec.rm_register] = register_map_file[exec.reg_register];
		} else if (exec.opcode == 0x8b) {
			register_map_file[exec.reg_register] = register_map_file[exec.rm_register];
		}
	} else {
#ifdef SUDHIR        
		cout<<"In register to memory or memory 2 register move\n";
#endif        
		if (exec.opcode == 0x89) {
			write_back(exec.address, register_map_file[exec.reg_register]);
		} else if (exec.opcode == 0x8b) {
			if (cache_read_done) {
				if (dcopy.valid) {
					register_map_file[exec.reg_register] = dcopy.data;
				} else {
					cout<<"This should not happen"<<endl;
				}
				dcache_read_request = false;
				cache_read_done = false;
			} else {
				read_memory(exec.address);
			}
		}
	}
}

void CoreCSE502::modifyRIP(uint64_t jump_address)
{

    RIP = jump_address;
    sc_addr tempRIP = RIP;
    last_inst_address = RIP;
    last_inst_size = 0;

    if (RIP % 16 != 0)
        tempRIP = RIP & ~15;

    for (int port = 0; port < i_ports; ++port)
    {
        prev_tag[port] = 0;
        i_op[port] = READ;
        i_tagin[port] = tempRIP;
        i_addr[port] = tempRIP;
        prev_tag[port] = tempRIP;
    }
    jump_flag_for_decoder = true;

	// empty the instruction queue
	while (!inst_queue.empty()) {
		inst_queue.pop();
	}
	callq_reset();
}

// in this case the address of the function to be executed is the displacement relative to the rip
void CoreCSE502::handle_call_relative(struct execution_struct &exec) {
    register_map_file["0100"] -= 8;
	debug_stack.push(exec.rip);
    write_back(register_map_file["0100"], exec.rip);  
	uint64_t jump_address = exec.rip + exec.immediate;
#ifdef SUDHIR    
	cout << "Jump address "<<std::hex<<jump_address<<std::dec<<endl;
#endif
	modifyRIP(jump_address);
}

void CoreCSE502::handle_call_abs(struct execution_struct &exec) {
    register_map_file["0100"] -= 8;
	debug_stack.push(exec.rip);
    write_back(register_map_file["0100"], exec.rip);
	uint64_t jump_address = register_map_file[exec.rm_register];
//	register_map_file["0100"] = register_map_file["0100"] - 8;
	modifyRIP(jump_address);
}
void CoreCSE502::handle_ja(struct execution_struct &exec) {
	if ((flag.carry_flag == false) && (flag.zero_flag == false)) {
		uint64_t jump_address = exec.rip + exec.immediate;
		modifyRIP(jump_address);
	}
}

void CoreCSE502::handle_test(struct execution_struct &exec) {

    uint64_t result;
    if (exec.opcode == 0xf7) {
        result = register_map_file[exec.rm_register] & exec.immediate; 
    } else if (exec.opcode == 0x85) {
        result = register_map_file[exec.reg_register] & register_map_file[exec.rm_register];
    }

    if (result & (1ULL<<63) == 0) {
        flag.sign_flag = false;
    } else {
        flag.sign_flag = true;
    }

    if (result == 0) {
        flag.zero_flag = true;
    } else {
        flag.zero_flag = false;
    }

    flag.carry_flag = false;
    flag.overflow_flag = false;
}

void CoreCSE502::handle_je(struct execution_struct &exec) {
	if (flag.zero_flag) {
		uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
		modifyRIP(jump_address);
	}
}


void CoreCSE502::handle_jne(struct execution_struct &exec) {
	if (flag.zero_flag == false) {
		uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
		modifyRIP(jump_address);
	}
}


void CoreCSE502::handle_jb(struct execution_struct &exec) {
	if (flag.carry_flag) {
		uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
		modifyRIP(jump_address);
	}
}

void CoreCSE502::handle_jge(struct execution_struct &exec) {
    if (flag.sign_flag == flag.overflow_flag) {
        uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
        modifyRIP(jump_address);
    }
}

void CoreCSE502::handle_jle(struct execution_struct &exec) {
    if (flag.zero_flag == true || (flag.sign_flag != flag.overflow_flag)) {
        uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
        modifyRIP(jump_address);
    }
}

void CoreCSE502::handle_jl(struct execution_struct &exec) {
    if (flag.sign_flag != flag.overflow_flag) {
        uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
        modifyRIP(jump_address);
    }
}

void CoreCSE502::handle_jg(struct execution_struct &exec) {
    if (flag.zero_flag == false || (flag.sign_flag == flag.overflow_flag)) {
        uint64_t jump_address = exec.rip + exec.immediate;  // take care of negative offset
        modifyRIP(jump_address);
    }
}

void CoreCSE502::handle_retq() {
	//cout<<"The RSP value at return is "<<std::hex<<register_map_file["0100"]<<std::dec<<endl;
	//cout<<"cache read done flag is "<<cache_read_done<<endl;
    if (cache_read_done) {
	//cout<<"how can you reach here"<<endl;
        register_map_file["0100"] = register_map_file["0100"] + 8;
	debug_stack_print();
		if (debug_stack.empty()) {
			cout<<"Debug stack empty\n";
		} else {
			debug_stack.pop();
		}
        if (dcopy.valid) {
            uint64_t return_address = dcopy.data;
            modifyRIP(return_address);
        } else {
            cout << "Dont go here!!\n";
        }
        dcache_read_request = false;
	cache_read_done = false;
    } else {
	//cout<<"The RSP value at return is "<<std::hex<<register_map_file["0100"]<<std::dec<<endl;
        read_memory(register_map_file["0100"]);
    }
}

void CoreCSE502::handle_syscall(struct execution_struct &exec) {
	uint64_t return_value = (uint64_t)syscall(exec.rip, register_map_file["0100"],
		register_map_file["0000"], register_map_file["0111"], 
		register_map_file["0110"], register_map_file["0010"],
		register_map_file["1010"], register_map_file["1000"],
		register_map_file["1001"]);

    register_map_file["0000"] = return_value;

#ifdef SUDHIR
	cout<<"Back from syscall\n";
#endif
}

void CoreCSE502::write_back(uint64_t address, uint64_t data) {
	for (int port = 0; port < d_ports; ++port) {
		d_op[port] = WRITE;
		d_tagin[port] = address;
		d_addr[port] = address;
#ifdef SUDHIR        
		cout <<"writing to dcache \n";
#endif
		d_dout[port].write(data);
	}
}
/*
if (d_cache_state == Read_State)
    {
        for (int port = 0; port < i_ports; ++port)
            if (!d_ready[port])
                return;
            else
            {
                sc_addr temp_read = d_data[port].read();
                cout << "Data read from d_cache : "<<std::hex << temp_read << std::dec<<endl;
                d_cache_state = Normal_State;

                do
                {
                    d_op[port] = WRITE;
                    d_tagin[port] = 67107840 -8;
                    d_addr[port] = 67107840-8;
                    cout <<"writing to dcache \n";
                    d_dout[port].write(0x0102030405060708);
                }while(0);
            }
    }


 for (int port = 0; port < i_ports; ++port)
    {
        d_op[port] = READ;
        d_tagin[port] = 67107840;
        d_addr[port] = 67107840;
        d_cache_state = Read_State;
    }
*/

uint64_t CoreCSE502::read_memory(uint64_t address) {
	
	for (int port = 0;port < d_ports; ++port) {
		d_op[port] = READ;
		d_tagin[port] = address;
		d_addr[port] = address;
	}
	dcache_read_request = true;
}

void CoreCSE502::handle_immed_move(struct execution_struct &exec) {
#ifdef SUDHIR    
	cout<<"In in immediate move, the value to be copied is "<<exec.immediate<<endl;
#endif
	if ((exec.opcode >= 0xb8) && (exec.opcode <= 0xbf)) {
		uint8_t register_num = exec.opcode - 0xb8;
		if ((exec.rex_byte & 0x01) == 1) {
			register_num += 8;
		}
		register_map_file[register_string[register_num]] = exec.immediate;
	} else {
		register_map_file[exec.rm_register] = exec.immediate;
	}

}

void CoreCSE502::dump_registers() {
#ifdef VINAY
	cout<<"______________________________________________________________________________________________________________________________________"<<endl;
	cout<<"rax\t\t"<<std::hex<<register_map_file["0000"]<<endl;
	cout<<"rbx\t\t"<<register_map_file["0011"]<<endl;
	cout<<"rcx\t\t"<<register_map_file["0001"]<<endl;
	cout<<"rdx\t\t"<<register_map_file["0010"]<<endl;
	cout<<"rsi\t\t"<<register_map_file["0110"]<<endl;
	cout<<"rdi\t\t"<<register_map_file["0111"]<<endl;
	cout<<"rbp\t\t"<<register_map_file["0101"]<<endl;
	cout<<"rsp\t\t"<<register_map_file["0100"]<<endl;
	cout<<"r8\t\t"<<register_map_file["1000"]<<endl;
	cout<<"r9\t\t"<<register_map_file["1001"]<<endl;
	cout<<"r10\t\t"<<register_map_file["1010"]<<endl;
	cout<<"r11\t\t"<<register_map_file["1011"]<<endl;
	cout<<"r12\t\t"<<register_map_file["1100"]<<endl;
	cout<<"r13\t\t"<<register_map_file["1101"]<<endl;
	cout<<"r14\t\t"<<register_map_file["1110"]<<endl;
	cout<<"r15\t\t"<<register_map_file["1111"]<<std::dec<<endl;
	cout<<"______________________________________________________________________________________________________________________________________"<<endl;
	cout<<std::dec;

    cout<<"______________________________________________________________________________________________________________________________________"<<endl;
    cout<< "Carry flag : "<<flag.carry_flag << "\t"<< "Zero flag : "<<flag.zero_flag << "\t"<< "Sign flag : "<<flag.sign_flag << "\t"<< "Overflow flag : "<<flag.overflow_flag << "\t";
    cout<<endl<<"______________________________________________________________________________________________________________________________________"<<endl;



#endif
}

void CoreCSE502::debug_stack_print() {

		//cout<<std::hex<<debug_stack<<std::dec<<endl;
	//cout<<endl;
}
